package com.newsletter.invokeJSONserver;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import com.newsletter.pdfGenerator.DashBoardUtilityImpl;
import com.newsletter.pojos.CFConstants;
import com.newsletter.widgets.Grid;
import org.apache.log4j.Logger;

public class HeaderLogo {

    static Logger log = Logger.getLogger(Grid.class.getName());

    /**
     * Inner class to add a table as header.
     */
    public class TableHeader extends PdfPageEventHelper {

        /**
         * The header text.
         */
        String header;

        /**
         * The template with the total number of pages.
         */
        PdfTemplate total;

        /**
         * Allows us to change the content of the header.
         *
         * @param header The new header String
         */
        public void setHeader(String header) {
            this.header = header;
        }

        /**
         * Creates the PdfTemplate that will hold the total number of pages.
         *
         * @see
         * com.itextpdf.text.pdf.PdfPageEventHelper#onOpenDocument(com.itextpdf.text.pdf.PdfWriter,
         * com.itextpdf.text.Document)
         */
        public void onOpenDocument(PdfWriter writer, Document document) {
            total = writer.getDirectContent().createTemplate(30, 16);
        }

        /**
         * Adds a header to every page
         *
         * @see
         * com.itextpdf.text.pdf.PdfPageEventHelper#onEndPage(com.itextpdf.text.pdf.PdfWriter,
         * com.itextpdf.text.Document)
         */
        public void onEndPage(PdfWriter writer, Document document) {

            //ResourceBundle bundle = ResourceBundle.getBundle("properties/config");
            String URL = CFConstants.BUNDLE.getString("URL");

            PdfPTable table = new PdfPTable(3);
            try {

                table.setWidths(new int[]{25, 15, 25}); // Title positions
                Date date = new Date();
                String formatedDate = new SimpleDateFormat(CFConstants.US_DATE_FORMAT).format(date);

                table.setTotalWidth(530);
                table.setLockedWidth(true);
                table.getDefaultCell().setFixedHeight(20);
                table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
                PdfPCell cell = null;
                BaseColor headerFontColor = WebColors.getRGBColor("#000000");
                Font font = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL, headerFontColor);
                PdfPCell cellDate = new PdfPCell(new Paragraph(CFConstants.CREATED_ON + CFConstants.COLON + CFConstants.SPACE + formatedDate, font));
                cellDate.setBorderWidth(0);
                cellDate.setHorizontalAlignment(Element.ALIGN_RIGHT);

                String imagePath;
                Image logoImage = null;

                try {
                    imagePath = URL + CFConstants.LOGO_PATH + DashBoardUtilityImpl.workSpaceObj.getCompanyName() + CFConstants.LOGO_NAME;
                    imagePath = imagePath.replace(" ", "%20");
                    logoImage = Image.getInstance(imagePath);

                } catch (IOException e) {
//					e.printStackTrace();
                    log.debug(e);
                    imagePath = URL + CFConstants.LOGO_PATH + CFConstants.DEFAULT_COMPANYNAME + CFConstants.LOGO_NAME;
                    try {
                        logoImage = Image.getInstance(imagePath);
                    } catch (IOException elogo) {
//                        elogo.printStackTrace();
                        log.debug(elogo);
                    }
                }
                if (logoImage != null) {
                    logoImage.scaleAbsolute(150f, 60f);
                    cell = new PdfPCell(logoImage);
                } else {
                    cell = new PdfPCell(new Paragraph(CFConstants.SPACE));
                }

                cell.setBorder(Rectangle.NO_BORDER);
                PdfPCell cName = new PdfPCell(new Paragraph(header));
                cName.setHorizontalAlignment(Element.ALIGN_CENTER);
                cName.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell);
                table.addCell(cName);
                table.addCell(cellDate);
                table.addCell(String.format("AHAIO"));
                table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);

                table.writeSelectedRows(0, -1, 34, 803, writer.getDirectContent());

            } catch (DocumentException de) {
                throw new ExceptionConverter(de);
            }
        }
    }
}
