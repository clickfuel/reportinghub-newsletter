package com.newsletter.invokeJSONserver;
/*
 * Author : Rishabh Prashar
 * 
 * This Class is responsible for getting the URL,
 * Returning the data - JSON of Widgets 
 * and call phantom server to get response
 * 
 * */

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Image;
import com.newsletter.pojos.CFConstants;
import org.apache.log4j.Logger;

public class JSONserverInvoker {

    static Logger log = Logger.getLogger(JSONserverInvoker.class.getName());

    public String callURL(String url) throws MalformedURLException, IOException {
        //System.out.println("WIdget URLn at CALL  -- >  " +url);
        log.debug("WIdget URLn at CALL  -- >  " + url);
        StringBuilder builder = new StringBuilder();
        String response = null;
        try {
            url = url.replace(CFConstants.SPACE, "%20");
            URL website = new URL(url);
            int calltry = 0;
            HttpURLConnection connection = openConnection(website);
//            connection.setConnectTimeout(3000000);
            if(connection.getResponseCode() == 200){
                try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                    String inputLine;
                    while ((inputLine = bufferedReader.readLine()) != null) {
                        builder.append(inputLine);
                    }
                }           
                response = String.valueOf(builder); 
            }
        } catch (MalformedURLException e) {
            response = null;
        } catch (IOException e) {
            response = null;
        } catch (Exception e) {
            response = null;
        }

        //System.out.println("JSON== " + response.toString());
        log.debug("JSON-->" + response);
        return response;
    }

    public  Image pingServer(String jsonString) throws IOException, BadElementException {

        Image returnImage = null;
        String PHANTOMJS_URL = CFConstants.BUNDLE.getString("PHANTOMJS_URL");  // Fetch content data from given URL
        // Configuring POST Request			
        URL url = new URL(PHANTOMJS_URL);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        int calltry = 0;
        connection.setRequestMethod("POST");
        connection.setRequestProperty("User-Agent", CFConstants.USER_AGENT);
        connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        // Setting Parameter "option"		
        String urlParameters = CFConstants.OPTIONS + CFConstants.EQUALS + jsonString;
        // Send post request
        connection.setDoOutput(true);
        DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());
        dataOutputStream.writeBytes(urlParameters);
        dataOutputStream.flush();
        dataOutputStream.close();
        while (connection.getResponseCode() == 500 && calltry < 5) {
            try {
                Thread.sleep(2000);
                calltry++;
                connection = openConnection(url);
               
                connection.setRequestMethod("POST");
                connection.setRequestProperty("User-Agent", CFConstants.USER_AGENT);
                connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

                // Setting Parameter "option"		
                urlParameters = CFConstants.OPTIONS + CFConstants.EQUALS + jsonString;
                // Send post request
                connection.setDoOutput(true);
                dataOutputStream = new DataOutputStream(connection.getOutputStream());
                dataOutputStream.writeBytes(urlParameters);
                dataOutputStream.flush();
                dataOutputStream.close();

            } catch (InterruptedException ex) {
                log.debug(ex);
            }
        }

        int responseCode = connection.getResponseCode();
        if(responseCode != 500){
        /*System.out.println("\nSending 'POST' request to URL : " + url);
         System.out.println("Post parameters : " + urlParameters);
         System.out.println("Response Code : " + responseCode);-*/

        log.debug("\nSending 'POST' request to URL : " + url);
        log.debug("Post parameters : " + urlParameters);
        log.debug("Response Code : " + responseCode);

        ImageInputStream imageInputStream = ImageIO.createImageInputStream(connection.getInputStream());

        BufferedImage bufferedImg = ImageIO.read(imageInputStream);

        returnImage = Image.getInstance(bufferedImg, null);
//				returnImage.set

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = bufferedReader.readLine()) != null) {
            response.append(inputLine);
        }

        bufferedReader.close();

    }return returnImage;
    }
    
    public HttpURLConnection openConnection(URL website) throws IOException{
     HttpURLConnection connection =  (HttpURLConnection) website.openConnection();
     return connection;
    }
}
