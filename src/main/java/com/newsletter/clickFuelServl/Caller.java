package com.newsletter.clickFuelServl; 
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse; 
import com.newsletter.pdfGenerator.DashBoardUtilityImpl; 
import org.apache.log4j.Logger;

/**
 * Servlet implementation class Caller
 */
public class Caller extends HttpServlet {

    static Logger log = Logger.getLogger(Caller.class.getName());
    private static final long serialVersionUID = 1L;

    /**
     * Default constructor.
     */
    public Caller() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        String userID = request.getParameter("userID");
        String pageName = request.getParameter("pageName");
        System.out.println("User ID --- > " + userID);
        PrintWriter writer = null;        

        OutputStream responseOutputStream = null;
        try {
//            PrintWriter writer = response.getWriter();
            DashBoardUtilityImpl dbUtility = new DashBoardUtilityImpl();
            ByteArrayOutputStream bytesStream = null;
            try {
                bytesStream = dbUtility.createPDF(userID, pageName);
            } catch (Exception e) {
                writer = response.getWriter();
                writer.print(e.getMessage());
                e.printStackTrace();
            }            
            if (bytesStream != null) {

                response.setContentType("application/pdf");
                response.addHeader("Content-Disposition", "attachment; filename=" + "pdf_newsletter.pdf");

                responseOutputStream = response.getOutputStream();
                byte[] bytes = bytesStream.toByteArray();
                responseOutputStream.write(bytes);
            } else {

//                writer.print("Returned JSON IS NOT COMPATIBLE");
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            if (writer != null) {
                writer.close();
            }
            //close the input/output streams
            if (responseOutputStream != null) {
                responseOutputStream.close();
            }
        }
    }

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub		
    }

}
