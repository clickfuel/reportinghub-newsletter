package com.newsletter.widgets;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.newsletter.pojos.CFConstants;
import com.newsletter.pojos.Widget;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Chart {

    public static BaseColor borderColor = WebColors.getRGBColor(CFConstants.THEME_BUNDLE.getString("BorderColor"));
    public static BaseColor HeaderBackgroundColor = WebColors.getRGBColor(CFConstants.THEME_BUNDLE.getString("HeaderBackgroundColor"));
    public static BaseColor ContentBackgroundColor = WebColors.getRGBColor(CFConstants.THEME_BUNDLE.getString("ContentBackgroundColor"));
    public static BaseColor TitleFontColor = WebColors.getRGBColor(CFConstants.THEME_BUNDLE.getString("TitleFontColor"));
    public static BaseColor ContentTextColor = WebColors.getRGBColor(CFConstants.THEME_BUNDLE.getString("ContentTextColor"));
    public static BaseColor title_Bottom_Color = WebColors.getRGBColor("#f3f3f3");

    /**
     *
     * @param Json
     * @return
     */
    public static String convertRegExp(String Json) {
        Json = Json.replace("!", " %21 ");
//        Json = Json.replace("#", " %23 ");
        Json = Json.replace("$", " %24 ");
        Json = Json.replace("%", " %25 ");
        Json = Json.replace("&", " %26 ");
        Json = Json.replace("*", " %2A ");
        Json = Json.replace("/", " %2F ");

        return Json;
    }

    /**
     *
     * @param miliSec
     * @return
     */
    public static String convertMiliToDate(String miliSec) {
        DateFormat formatter = new SimpleDateFormat("dd/MMM");

        long milliSeconds = Long.parseLong(miliSec);
        System.out.println(milliSeconds);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        String retDate = formatter.format(calendar.getTime());
        return retDate;
    }

    /**
     *
     * @param table
     * @param widget
     * @param colspan
     * @param minHeight
     * @return
     */
    public PdfPTable addFirstCell(PdfPTable table, Widget widget, int colspan, float minHeight) {
        Font font3 = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL);

        String headerTitle = widget.getWidgetTitle().trim();
        /*if (headerTitle.equals(CFConstants.EMPTY_STRING)) {
         //headerTitle = CFConstants.BIG_NUMBER;
         }*/
        if (headerTitle.trim().equals("")) {
            headerTitle = CFConstants.SPACE;
        }
        Font font = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL, TitleFontColor);
        if (headerTitle.trim().equals("")) {
            headerTitle = " ";
        }
        PdfPCell headerCell = new PdfPCell(new Paragraph(headerTitle, font));
        headerCell.setColspan(colspan);

        headerCell.setBorderColor(borderColor);
        headerCell.setPaddingBottom(2f);

        headerCell.setBackgroundColor(HeaderBackgroundColor);
        PdfPCell firstCell = new PdfPCell(new Paragraph(widget.getDefaultDate(), font3));
        firstCell.setColspan(colspan);
        firstCell.setBorderColor(borderColor);
        firstCell.setBorderWidthBottom(0);
        firstCell.setBorderWidthTop(0);
        firstCell.setBackgroundColor(ContentBackgroundColor);

        firstCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        firstCell.setMinimumHeight(minHeight);
        table.addCell(headerCell);
        table.addCell(firstCell);
        return table;
    }

    /**
     * Method to format numbers to 2 digits after decimal (if it is float or
     * double) and add comma after every 3 digits
     *
     * @param value
     * @return
     */
    public String formatNumber(String value) {
        String currentValue = value;
        try {
            double valueNumber = Double.parseDouble(value);

            if (value.contains(CFConstants.DOT)) {
                value = String.format("%,.2f", valueNumber);
                if (value.contains(".00")) {
                    value = String.format("%,.0f", valueNumber);
                }
            } else {
                value = String.format("%,.0f", valueNumber);
            }
        } catch (Exception e) {
            value = currentValue;
        }

        return value;
    }
}
