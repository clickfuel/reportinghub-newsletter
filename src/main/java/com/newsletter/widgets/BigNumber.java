/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.newsletter.widgets;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.newsletter.pdfGenerator.DashBoardUtilityImpl;
import com.newsletter.pojos.CFConstants;
import com.newsletter.pojos.Widget;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author graymatter
 */
public class BigNumber {

    static Logger log = Logger.getLogger(BigNumber.class.getName());
    Chart chart = new Chart();

    /**
     *
     * @param document
     * @param jsonString
     * @param title
     * @param widget
     * @return
     * @throws JSONException
     * @throws DocumentException
     */
    public PdfPTable createBigNumber(Document document, String jsonString, String title, Widget widget) throws JSONException,
            DocumentException {

        JSONObject jsnObj = new JSONObject(jsonString);

        PdfPTable table = new PdfPTable(1);

        table.setWidthPercentage(26f);

        BaseColor numberColor = Chart.ContentTextColor;
        BaseColor metricColor = Chart.ContentTextColor;

        if (widget.getBigNumberHeaderTextColor() != null && !widget.getBigNumberHeaderTextColor().equalsIgnoreCase(CFConstants.NULL)) {
            numberColor = WebColors.getRGBColor(widget.getBigNumberHeaderTextColor());
        }

        if (widget.getBigNumberNumberTextColor() != null && !widget.getBigNumberNumberTextColor().equalsIgnoreCase(CFConstants.NULL)) {
            metricColor = WebColors.getRGBColor(widget.getBigNumberNumberTextColor());
        }

        int numberFontSize = 0;
        int metricFontSize = 0;
        if (widget.getBigNumberHeaderTextSize() >= 20) {
            numberFontSize = 8;
        } else {
            numberFontSize = 5;
        }

        if (widget.getBigNumberNumberTextSize() >= 20) {
            metricFontSize = 10;
        } else {
            metricFontSize = 7;
        }
        Font font1 = new Font(Font.FontFamily.HELVETICA, widget.getBigNumberHeaderTextSize() - numberFontSize, Font.NORMAL, numberColor);
        Font font2 = new Font(Font.FontFamily.HELVETICA, widget.getBigNumberNumberTextSize() - metricFontSize, Font.NORMAL, metricColor);

        //System.out.println("BIG NUM - " + widget.getBigNumberHeaderTextSize() + " -- " + widget.getBigNumberNumberTextSize());
        String measure = null;
        String value = null;

        chart.addFirstCell(table, widget, 1, 60f);
        //dashboard.addFirstCell(table, widget);

        Iterator<?> keyOb1 = ((JSONObject) jsnObj).keys();

        String firstKey = String.valueOf(keyOb1.next());
        Object parentObj = jsnObj.get(firstKey);
        JSONObject childObj = (JSONObject) parentObj;
        Iterator<?> keys = childObj.keys();

        if (childObj.length() < 1) {

        }

        while (keys.hasNext()) {
            String key = (String) keys.next();

            if (key.equalsIgnoreCase(String.valueOf(DashBoardUtilityImpl.workSpaceObj.getCompanyID()))) {

                JSONObject innerData = (JSONObject) childObj.get(key);
                Iterator<?> innerKeys = innerData.keys();
                LinkedHashMap<String, String> llmap = new LinkedHashMap<>();
                while (innerKeys.hasNext()) {
                    measure = String.valueOf(innerKeys.next());
                    value = (String) innerData.get(measure);
                    llmap.put(measure, value);
                }

                List<Entry<String, String>> list = new ArrayList<>(llmap.entrySet());

                for (int i = list.size() - 1; i >= 0; i--) {
                    Entry<String, String> entry = list.get(i);

                    measure = entry.getKey();
                    value = entry.getValue();

                    value = chart.formatNumber(value);

                    PdfPCell secodCell = new PdfPCell(new Paragraph(value, font1));
                    PdfPCell thirdCell = new PdfPCell(new Paragraph(measure, font2));

                    secodCell.setBorderColor(Chart.borderColor);
                    secodCell.setBorderWidthBottom(0f);
                    secodCell.setBorderWidthTop(0f);
                    secodCell.setBackgroundColor(Chart.ContentBackgroundColor);

                    thirdCell.setBorderColor(Chart.borderColor);
                    thirdCell.setBorderWidthTop(0);
                    thirdCell.setBackgroundColor(Chart.ContentBackgroundColor);
                    thirdCell.setBorderWidthBottom(0);

                    secodCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    thirdCell.setHorizontalAlignment(Element.ALIGN_CENTER);

                    table.setWidthPercentage(100);
                    table.setSpacingAfter(10f);

                    table.addCell(secodCell);
                    table.addCell(thirdCell);
                }
                PdfPCell lastCell = new PdfPCell(new Paragraph(CFConstants.EMPTY_STRING));
                lastCell.setBorderColor(Chart.borderColor);
                lastCell.setBorderWidthTop(0f);
                if (innerData.length() < 2) {
                    lastCell.setMinimumHeight(44f);
                } else {
                    lastCell.setMinimumHeight(9f);
                }
                lastCell.setBackgroundColor(Chart.ContentBackgroundColor);
                table.addCell(lastCell);
            }
        }
        // document.add(table);
        return table;
    }

    public PdfPTable createBigNumber2(Document document, String jsonString, String title, Widget widget) throws JSONException,
            DocumentException {

        JSONArray jsnArray = new JSONArray(jsonString);

        PdfPTable table = new PdfPTable(1);

        table.setWidthPercentage(26f);

        BaseColor numberColor = Chart.ContentTextColor;
        BaseColor metricColor = Chart.ContentTextColor;

        if (widget.getBigNumberHeaderTextColor() != null && !widget.getBigNumberHeaderTextColor().equalsIgnoreCase(CFConstants.NULL)) {
            numberColor = WebColors.getRGBColor(widget.getBigNumberHeaderTextColor());
        }

        if (widget.getBigNumberNumberTextColor() != null && !widget.getBigNumberNumberTextColor().equalsIgnoreCase(CFConstants.NULL)) {
            metricColor = WebColors.getRGBColor(widget.getBigNumberNumberTextColor());
        }

        int numberFontSize = 0;
        int metricFontSize = 0;
        if (widget.getBigNumberHeaderTextSize() >= 20) {
            numberFontSize = 8;
        } else {
            numberFontSize = 5;
        }

        if (widget.getBigNumberNumberTextSize() >= 20) {
            metricFontSize = 10;
        } else {
            metricFontSize = 7;
        }
        Font font1 = new Font(Font.FontFamily.HELVETICA, widget.getBigNumberHeaderTextSize() - numberFontSize, Font.NORMAL, numberColor);
        Font font2 = new Font(Font.FontFamily.HELVETICA, widget.getBigNumberNumberTextSize() - metricFontSize, Font.NORMAL, metricColor);

        //System.out.println("BIG NUM - " + widget.getBigNumberHeaderTextSize() + " -- " + widget.getBigNumberNumberTextSize());
        String measure = null;
        String value = null;
        int compId = 0;
        chart.addFirstCell(table, widget, 1, 50f);
        if (jsnArray.length() == 1) {
            JSONArray nameArray = jsnArray.getJSONArray(0);
            for (int i = 0; i < nameArray.length(); i++) {
                String name = "";
                if (!String.valueOf(nameArray.get(i)).equalsIgnoreCase("Company ID")) {
                    name = String.valueOf(nameArray.get(i));

                    PdfPCell secodCell = new PdfPCell(new Paragraph("-", font1));
                    PdfPCell thirdCell = new PdfPCell(new Paragraph(name, font2));

                    secodCell.setBorderColor(Chart.borderColor);
                    secodCell.setBorderWidthBottom(0f);
                    secodCell.setBorderWidthTop(0f);
                    secodCell.setBackgroundColor(Chart.ContentBackgroundColor);

                    thirdCell.setBorderColor(Chart.borderColor);
                    thirdCell.setBorderWidthTop(0);
                    thirdCell.setBackgroundColor(Chart.ContentBackgroundColor);
                    thirdCell.setBorderWidthBottom(0);

                    secodCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    thirdCell.setHorizontalAlignment(Element.ALIGN_CENTER);

                    table.setWidthPercentage(100);
                    table.setSpacingAfter(10f);

                    table.addCell(secodCell);
                    table.addCell(thirdCell);
                    PdfPCell lastCell = new PdfPCell(new Paragraph(CFConstants.EMPTY_STRING));
                    lastCell.setBorderColor(Chart.borderColor);
                    lastCell.setBorderWidthTop(0f);
                    lastCell.setMinimumHeight(44f);

                    lastCell.setBackgroundColor(Chart.ContentBackgroundColor);
                    table.addCell(lastCell);
                }

            }
        } else if (jsnArray.length() == 2) {
            JSONArray namesArray = jsnArray.getJSONArray(0);
            JSONArray valueArray = jsnArray.getJSONArray(1);
            LinkedHashMap<String, String> llmap = new LinkedHashMap<>();

            if (namesArray.length() == valueArray.length()) {
                for (int n = 0; n < namesArray.length(); n++) {
                    if (!String.valueOf(namesArray.get(n)).equalsIgnoreCase("Company ID")) {

                        measure = String.valueOf(namesArray.get(n));
                        value = String.valueOf(valueArray.get(n));
                        if (String.valueOf(valueArray.get(n)).equalsIgnoreCase(CFConstants.NULL)) {
                            value = "0";
                        }
                        value = chart.formatNumber(value);

                        PdfPCell secodCell = new PdfPCell(new Paragraph(value, font1));
                        PdfPCell thirdCell = new PdfPCell(new Paragraph(measure, font2));

                        secodCell.setBorderColor(Chart.borderColor);
                        secodCell.setBorderWidthBottom(0f);
                        secodCell.setBorderWidthTop(0f);
                        secodCell.setBackgroundColor(Chart.ContentBackgroundColor);

                        thirdCell.setBorderColor(Chart.borderColor);
                        thirdCell.setBorderWidthTop(0);
                        thirdCell.setBackgroundColor(Chart.ContentBackgroundColor);
                        thirdCell.setBorderWidthBottom(0);

                        secodCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        thirdCell.setHorizontalAlignment(Element.ALIGN_CENTER);

                        table.setWidthPercentage(100);
                        table.setSpacingAfter(10f);

                        table.addCell(secodCell);
                        table.addCell(thirdCell);

                    }
                }
                PdfPCell lastCell = new PdfPCell(new Paragraph(CFConstants.EMPTY_STRING));
                lastCell.setBorderColor(Chart.borderColor);
                lastCell.setBorderWidthTop(0f);
                if (namesArray.length() < 2) {
                    lastCell.setMinimumHeight(44f);
                } else {
                    lastCell.setMinimumHeight(9f);
                }
                lastCell.setBackgroundColor(Chart.ContentBackgroundColor);
                table.addCell(lastCell);
            } else {
// When there's name but no value
            }

        }

        return table;
    }

}
