/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.newsletter.widgets;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font; 
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.newsletter.pojos.CFConstants;
import com.newsletter.pojos.Widget;
import java.io.FileNotFoundException;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;

/**
 *
 * @author graymatter
 */
public class Grid {

    static Logger log = Logger.getLogger(Grid.class.getName());
    Chart chart = new Chart();

    /**
     * Method to create table and add to document
     *
     * @param document
     * @param jsonString
     * @param title
     * @param widget
     * @return
     * @throws JSONException
     * @throws FileNotFoundException
     * @throws DocumentException
     */
    public PdfPTable createTable(Document document, String jsonString, Widget widget) throws JSONException,
            FileNotFoundException, DocumentException {

        JSONArray jsnArray = null;
        PdfPTable table = null;
        
        try {
            if (!jsonString.trim().equals("")) {
                jsnArray = new JSONArray(jsonString);

                /*
                 * Creating PDF Document This PDF will be generated in specified
                 * location of fileOutput Stream
                 */
                int size = ((JSONArray) jsnArray.get(0)).length();
                if (size > 0) {
                    table = new PdfPTable(size);
                    JSONArray header = ((JSONArray) jsnArray.get(0));
                    
                    chart.addFirstCell(table, widget, size, 25f);

                    for (int i = 0; i < header.length(); i++) {
                        insertTableHeader(table, header.getString(i));
                    }

                    for (int j = 1; j < jsnArray.length(); j++) {
                        JSONArray row = ((JSONArray) jsnArray.get(j));
                        for (int i = 0; i < row.length(); i++) {
                            String value = String.valueOf(row.get(i));
                            if (value != null && !value.equals(CFConstants.NULL)) {
                                value = chart.formatNumber(value);
                                insertCell(table, value);
                            } else {
                                insertCell(table, CFConstants.EMPTY_STRING);
                            }

                        }
                        if (widget.getWidgetGridSize() == j) {
                            break;
                        }
                    }
                    if (jsnArray.length() == 1) {
                        for (int i = 0; i < header.length(); i++) {
                            insertCell(table, CFConstants.GRID_NO_DATA);
                        }
                    }
                }

            } else {

                table = new PdfPTable(1);
                PdfPCell headercell = new PdfPCell(new Paragraph(widget.getWidgetTitle()));
                headercell.setBackgroundColor(Chart.ContentBackgroundColor);
                PdfPCell cell = new PdfPCell(new Paragraph(CFConstants.NO_DATA_AVAILABLE));
                cell.setBackgroundColor(Chart.ContentBackgroundColor);
                table.addCell(headercell);
                headercell.addElement(cell);

            }
        } catch (Exception e) {
            //System.out.println("ERROR---->" + e.getMessage());
            log.debug("ERROR---->" + e.getMessage());
        }
        return table;

    }

    /*
     * This method will create Table Header,with background color, font type and
     * alignment settings,aligning text in cell
     */
    public PdfPTable insertTableHeader(PdfPTable table, String val) {

        Font headerFont = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, Chart.ContentTextColor);
        PdfPCell cellobj1 = new PdfPCell(new Paragraph(val, headerFont));
        cellobj1.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellobj1.setBackgroundColor(Chart.ContentBackgroundColor);
        cellobj1.setBorderColor(Chart.borderColor);
        table.addCell(cellobj1);

        return table;
    }

    /*
     * This method will create table data columns, font type and alignment
     * settings, Generating Font for all Column Data, setting width of column
     */
    public PdfPTable insertCell(PdfPTable table, String val) {

        Font dataFont = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, Chart.ContentTextColor);
        PdfPCell cellobj1 = new PdfPCell(new Paragraph(val, dataFont));
        cellobj1.setBorderColor(Chart.borderColor);
        cellobj1.setBackgroundColor(Chart.ContentBackgroundColor);
        table.addCell(cellobj1);
        table.setWidthPercentage(100);
        //table.setSpacingAfter(20);
        return table;
    }

}
