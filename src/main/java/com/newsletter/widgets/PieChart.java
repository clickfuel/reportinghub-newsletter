package com.newsletter.widgets;

import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import com.newsletter.pojos.CFConstants;
import com.newsletter.pojos.Widget;
import java.util.List;
import java.util.MissingResourceException;
import org.apache.log4j.Logger;

public class PieChart {

    static Logger log = Logger.getLogger(PieChart.class.getName());
    Chart chart = new Chart();

    public String getPieChart(String jSON, Widget widget) throws MissingResourceException {

        String width = String.valueOf(widget.getSizeX() * 150);
        String returnJson = null;
        StringBuilder preFix = new StringBuilder();
        String chart = CFConstants.WIDGET_BUNDLE.getString("CHART");
        //String options3d = CFConstants.WIDGET_BUNDLE.getString("PIE_CHART_3D");
        String pieStructure = CFConstants.WIDGET_BUNDLE.getString("PIE_CHART_STRUCTURE");
        String closeBracket = CFConstants.WIDGET_BUNDLE.getString("CLOSE_BRACKET");
        String highChartColors = CFConstants.THEME_BUNDLE.getString("highChartColors");

        pieStructure = pieStructure.replace("CHART_WIDTH", width);
        String donutStructure = CFConstants.WIDGET_BUNDLE.getString("DONUT_CHART_STRUCTURE");
        donutStructure = donutStructure.replace("CHART_WIDTH", width);
        preFix.append(chart);
        /*if (widget.getWidgetType().contains(CFConstants.PIE_3D) || widget.getWidgetType().contains(CFConstants.DONUT_3D)) {
         preFix.append(options3d);
         }*/

        if (widget.getOptions3d() != null && !widget.getOptions3d().equalsIgnoreCase(CFConstants.NULL)) {
            preFix.append(CFConstants.WIDGET_OPTIONS3D);
            preFix.append(CFConstants.COLON);
            preFix.append(widget.getOptions3d());
            preFix.append(CFConstants.COMMA);
        }
        if (widget.getWidgetType().contains(CFConstants.PIE_CHART) || widget.getWidgetType().contains("pie")
                || widget.getWidgetType().contains("Pie")) {
            preFix.append(pieStructure);
        } else if (widget.getWidgetType().contains(CFConstants.DONUT) || widget.getWidgetType().contains("Donut")
                || widget.getWidgetType().contains("donut")) {
            preFix.append(donutStructure);
        }

        try {
            JSONObject jsnObj = new JSONObject(jSON);
            Iterator<?> keyOb1 = ((JSONObject) jsnObj).keys();
            
            String firstKey = String.valueOf(keyOb1.next());
            Object parentObj = jsnObj.get(firstKey);
            JSONObject childObj = (JSONObject) parentObj;

            JSONObject dataObj = (JSONObject) childObj;

            StringBuilder chartData = new StringBuilder();
            Iterator<?> keys = childObj.keys();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                JSONObject finalObj = (JSONObject) dataObj.get(key);

                Iterator<?> finalKey = finalObj.keys();
                while (finalKey.hasNext()) {
                    String keyF = (String) finalKey.next();
                    chartData.append("[\"" + key + "-" + keyF + "\",");
//                    chartData.append("[\"" + keyF + "\",");
                    if (String.valueOf(finalObj.get(keyF)).trim().equals("")) {
                        chartData.append("0" + "],");
                    } else {
                        chartData.append(finalObj.get(keyF) + "],");
                    }

                }
            }
            if (!String.valueOf(chartData).isEmpty() && String.valueOf(chartData).contains(",")) {
                chartData.deleteCharAt(chartData.lastIndexOf(","));
            }

            preFix.append("[").append(String.valueOf(chartData)).append("]}]");
            preFix.append(CFConstants.COMMA).append(CFConstants.COLORS).append(CFConstants.COLON);

            returnJson = String.valueOf(preFix);
            returnJson = Chart.convertRegExp(returnJson);
            returnJson = returnJson + highChartColors + closeBracket;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //System.out.println("-PIE convertToPieChart-------------------->   " + returnJson);
        log.debug("-PIE convertToPieChart---->   " + returnJson);
        //returnJson = Chart.convertRegExp(returnJson);        
        return returnJson;

    }

    /**
     *
     * @param widgt
     * @param widgetID
     * @param retrnImage
     * @return
     */
    public PdfPCell pieImageSet(Widget widgt, List<String> widgetID, Image retrnImage) {

        PdfPTable pieTable = new PdfPTable(1);
        pieTable.setSplitLate(false);

        chart.addFirstCell(pieTable, widgt, 1, 20f);
        PdfPCell imgCell = new PdfPCell(retrnImage, true);
        imgCell.setMinimumHeight(137f);
        imgCell.setBackgroundColor(Chart.ContentBackgroundColor);
        imgCell.setBorderWidthTop(0f);
        imgCell.setBorderColor(Chart.borderColor);
        imgCell.setPadding(1f);

        PdfPCell firstCell = new PdfPCell(new Paragraph(CFConstants.EMPTY_STRING));
        firstCell.setPadding(4f);
        firstCell.setBorderWidth(0);
        firstCell.setMinimumHeight(173f);
        pieTable.addCell(imgCell);
        pieTable.setWidthPercentage(100);
        firstCell.addElement(pieTable);
        return firstCell;
    }

    public PdfPCell donutImageSet(Widget widgt, List<String> widgetID, Image retrnImage) {

        String headerTitle = widgt.getWidgetTitle().trim();

        Font font = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL, Chart.TitleFontColor);
        if (headerTitle.trim().equals("")) {
            headerTitle = CFConstants.SPACE;
        }
        PdfPCell headerCell = new PdfPCell(new Paragraph(headerTitle, font));
        headerCell.setBackgroundColor(Chart.HeaderBackgroundColor);
        headerCell.setBorderColor(Chart.borderColor);

        PdfPTable innerTable = new PdfPTable(1);

        PdfPCell imgCell = new PdfPCell(retrnImage, true);
        imgCell.setMinimumHeight(137f);
        imgCell.setBackgroundColor(Chart.ContentBackgroundColor);
        imgCell.setBorderColor(Chart.borderColor);
        imgCell.setPadding(2f);
        imgCell.setBorderWidthTop(0f);

        PdfPCell firstCell = new PdfPCell(new Paragraph(CFConstants.EMPTY_STRING));
        firstCell.setBorderWidth(0);
        firstCell.setMinimumHeight(185f);
        innerTable.addCell(headerCell);
        innerTable.addCell(imgCell);
        innerTable.setWidthPercentage(100);
        innerTable.setSpacingAfter(10f);
        firstCell.addElement(innerTable);
        return firstCell;
    }

}
