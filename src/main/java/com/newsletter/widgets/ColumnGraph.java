package com.newsletter.widgets;

import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import java.util.Iterator;

import org.json.JSONObject;

import com.newsletter.pojos.CFConstants;
import com.newsletter.pojos.Widget;
import java.util.List;
import java.util.MissingResourceException;
import org.apache.log4j.Logger;
import org.json.JSONArray;

public class ColumnGraph {

    static Logger log = Logger.getLogger(ColumnGraph.class.getName());
    Chart chart = new Chart();

    public String getColumnGraph(String jSON, Widget widget) throws MissingResourceException {
        String returnJson = "";
        String width = String.valueOf(widget.getSizeX() * 100);
        String structure = CFConstants.WIDGET_BUNDLE.getString("COLUMN_STRUCTURE");
        String highChartColors = CFConstants.THEME_BUNDLE.getString("highChartColors");

        structure = structure.replace("CHART_WIDTH", width);
        String series = null;
        String categories = null;

        JSONObject jsnObj = new JSONObject(jSON);
        Iterator<?> seriesItr = ((JSONObject) jsnObj).keys();
        while (seriesItr.hasNext()) {
            String key = (String) seriesItr.next();
            if (key.equalsIgnoreCase(CFConstants.CHART_SERIES)) {
                series = key + CFConstants.COLON + String.valueOf(jsnObj.get(key));
            } else if (key.equalsIgnoreCase(CFConstants.CHART_CATEGORY)) {
                categories = key + CFConstants.COLON + String.valueOf(jsnObj.get(key));
            }
        }
        JSONArray catArray = new JSONArray(series);
        String charLength = "3";
        if (catArray.length() < 3) {
            charLength = "7";
        } else {
            charLength = "3";
        }

        structure = structure.replace("WIDGET_SUBTYPE", widget.getWidgetSubtype());
//        structure = structure.replace("WIDGET_TITLE", widget.getWidgetTitle());
        structure = structure.replace("WIDGET_TITLE", "");
        structure = structure.replace("CATAGORIES", categories);
        structure = structure.replace("SERIES", series);
        structure = structure.replace("CHAR_LENGTH", charLength);
        returnJson = Chart.convertRegExp(structure);

        returnJson = returnJson.replace("HIGHCHART_COLORS", highChartColors);

        //System.out.println("COLUMN-JSON---?? - " + structure);
        log.debug("COLUMN-JSON---> " + returnJson);

        return returnJson;

    }

    /**
     *
     * @param widgt
     * @param widgetID
     * @param retrnImage
     * @return
     */
    public PdfPCell columnImageSet(Widget widgt, List<String> widgetID, Image retrnImage) {
        PdfPTable innerTable = new PdfPTable(1);
        chart.addFirstCell(innerTable, widgt, 1, 20f);
        PdfPCell imgCell = new PdfPCell(retrnImage, true);
        imgCell.setMinimumHeight(137f);
        imgCell.setBackgroundColor(Chart.ContentBackgroundColor);
        imgCell.setBorderColor(Chart.borderColor);
        imgCell.setBorderWidthTop(0f);
        imgCell.setPadding(2f);

        PdfPCell firstCell = new PdfPCell(new Paragraph(CFConstants.EMPTY_STRING));
        firstCell.setPadding(4f);
        firstCell.setBorderWidth(0);
        innerTable.addCell(imgCell);
        innerTable.setWidthPercentage(100);
        innerTable.setSpacingAfter(10f);
        firstCell.addElement(innerTable);
        return firstCell;
    }

}
