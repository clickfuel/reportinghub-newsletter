package com.newsletter.widgets;

import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.newsletter.pojos.CFConstants;
import com.newsletter.pojos.Widget;
import java.util.List;
import java.util.MissingResourceException;
import org.apache.log4j.Logger;

public class AreaGraph {

    static Logger log = Logger.getLogger(AreaGraph.class.getName());
Chart chart = new Chart();
    public String convertToAreaChart(String jSON, Widget widget) throws MissingResourceException {

        String returnJson = "";
        StringBuilder prefix = new StringBuilder();
        String width = String.valueOf(widget.getSizeX() * 100);
        String chart = CFConstants.WIDGET_BUNDLE.getString("CHART");
        String structure = CFConstants.WIDGET_BUNDLE.getString("AREA_STRUCTURE");
        structure = structure.replace("CHART_WIDTH", width);
        String stack = CFConstants.WIDGET_BUNDLE.getString("AREA_STACK");
        String series = CFConstants.WIDGET_BUNDLE.getString("AREA_SERIES");
        String closeBracket = CFConstants.WIDGET_BUNDLE.getString("CLOSE_BRACKET");
        String invertedType = CFConstants.WIDGET_BUNDLE.getString("LINE_INVERTED_TYPE");
        String highChartColors = CFConstants.THEME_BUNDLE.getString("highChartColors");

        structure = structure.replace("WIDGET_SUBTYPE", widget.getWidgetSubtype());
//        structure = structure.replace("WIDGET_TITLE", widget.getWidgetTitle());
        structure = structure.replace("WIDGET_TITLE", "");

        prefix.append(chart);

        if (String.valueOf(widget.getWidgetType()).equalsIgnoreCase(CFConstants.LINE_INVERTED_AXIS)) {
            prefix.append(invertedType);
        }

        prefix.append(structure);

        if (String.valueOf(widget.getWidgetType()).contains(CFConstants.AREA_GRAPH_STACKED)) {
            prefix.append(stack);
        }

        prefix.append(series);
        prefix.append(jSON);

        //prefix.append(CFConstants.COMMA).append(CFConstants.COLORS).append(CFConstants.COLON).append(highChartColors); //Highchart Colors
        prefix.append(closeBracket);

        returnJson = Chart.convertRegExp(String.valueOf(prefix));
        returnJson = returnJson.replace("HIGHCHART_COLORS", highChartColors);
        //System.out.println("convert To Area-Range-Spline-Stacked Chart - " + prefix.toString());
        log.debug("convert To Area-Range-Spline-Stacked Chart - " + returnJson);
        return returnJson;

    }

    /**
     *
     * @param widgt
     * @param widgetID
     * @param retrnImage
     * @return
     */
    public PdfPCell lineImageSet(Widget widgt, List<String> widgetID, Image retrnImage) {
        PdfPTable pieTable = new PdfPTable(1);
        chart.addFirstCell(pieTable, widgt, 1, 20f);
        
        pieTable.setWidthPercentage(100);
        pieTable.setSplitLate(false);
        PdfPCell imgCell = new PdfPCell(retrnImage, true);
        imgCell.setBackgroundColor(Chart.ContentBackgroundColor);
        imgCell.setBorderColor(Chart.borderColor);
        imgCell.setPadding(2f);
        imgCell.setBorderWidthTop(0f);
        
        PdfPCell firstCell = new PdfPCell(new Paragraph(CFConstants.EMPTY_STRING));
        pieTable.addCell(imgCell);
        pieTable.setSpacingAfter(10f);
        firstCell.addElement(pieTable);
        firstCell.setBorderWidth(0);
        return firstCell;
    }

    public PdfPCell areaImageSet(Widget widgt, List<String> widgetID, Image retrnImage) {
        PdfPTable pieTable = new PdfPTable(1);
        pieTable.setWidthPercentage(100);
        pieTable.setSplitLate(false);
        chart.addFirstCell(pieTable, widgt, 1, 20f);
        
        PdfPCell imgCell = new PdfPCell(retrnImage, true);
        imgCell.setMinimumHeight(137f);
        imgCell.setBackgroundColor(Chart.ContentBackgroundColor);
        imgCell.setBorderColor(Chart.borderColor);
        imgCell.setPadding(2f);
        imgCell.setBorderWidthTop(0f);
        
        PdfPCell firstCell = new PdfPCell(new Paragraph(CFConstants.EMPTY_STRING));
        pieTable.addCell(imgCell);
        pieTable.setSpacingAfter(10f);
        firstCell.addElement(pieTable);
        firstCell.setBorderWidth(0);
        return firstCell;

    }

}
