/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.newsletter.widgets;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.newsletter.pojos.CFConstants;
import com.newsletter.pojos.Widget;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;

/**
 *
 * @author graymatter
 */
public class TopKeywords {

    static Logger log = Logger.getLogger(TopKeywords.class.getName());
    Chart chart = new Chart();

    /**
     * Method to get the Top Keyword data
     *
     * @param Json
     * @param widget
     * @return
     * @throws JSONException
     * @throws com.itextpdf.text.DocumentException
     */
    @SuppressWarnings("rawtypes")
    public PdfPTable createTopKeyword(String Json, Widget widget) throws JSONException, DocumentException, NumberFormatException {

        int colspan = 1;

        if (widget.getIsChecked()) {
            colspan = 2;
        } else {
            //chart.addFirstCell(headerTable, widget, 1);
            colspan = 1;
        }

        PdfPTable headerTable = new PdfPTable(colspan);
        headerTable.setWidthPercentage(80f);
        //headerTable.setWidths(new int[]{colspan, 1});
        chart.addFirstCell(headerTable, widget, colspan, 25f);

        /*if (widget.getWidgetTitle().trim().equals(CFConstants.EMPTY_STRING)) {
         widget.setWidgetTitle(CFConstants.TOP_KEYWORDS);
         }*/
        //chart.addFirstCell(headerTable, widget);
        Map<String, Double> keywordMap = new HashMap<>();
        //Map<String, Double> sortedMap = new LinkedHashMap<>();
        Map<Object, Object> originalKeywordMap = new HashMap<>();
        JSONArray jsnArray = null;
        PdfPTable table = null;
        try {

            jsnArray = new JSONArray(Json);
            int size = ((JSONArray) jsnArray.get(0)).length();
            if (size > 0) {
                table = new PdfPTable(size);
                for (int j = 1; j < jsnArray.length(); j++) {
                    JSONArray row = ((JSONArray) jsnArray.get(j));
                    String key = null;
                    double value = 0;
                    Object originalKey = null;
                    Object originalValue = null;

                    for (int i = 0; i < row.length(); i++) {

                        if (i == 0) {
//                            if (!String.valueOf(row.get(i)).trim().equals("null")) {
                                key = String.valueOf(row.get(i));
                                originalKey = row.get(i);
//                            }
                        } else {
//                            if (!String.valueOf(row.get(i)).trim().equals("null")) {
                                originalValue = row.get(i);
                                String valString = String.valueOf(row.get(i));
                                if (row.get(i) == null || String.valueOf(row.get(i)).trim().equalsIgnoreCase(CFConstants.NULL)) {
                                    value = 0;
                                } else {
                                    value = Double.parseDouble(valString);
//                                }
                            }
                        }
                    }
                    originalKeywordMap.put(originalKey, originalValue);
                    keywordMap.put(key, value);
                }
            }
        } catch (JSONException | NumberFormatException e) {

            log.debug("ERROR---->" + e);
        }

        if (widget.getSortBy().equalsIgnoreCase(CFConstants.TOPKEYWORDS_LOWEST_TO_HIGHEST)) {
            keywordMap = sortByValuesAscendingOrder(keywordMap);
        } else if (widget.getSortBy().equalsIgnoreCase(CFConstants.TOPKEYWORDS_HIGHEST_TO_LOWEST)) {
            keywordMap = sortByValuesDescendingOrder(keywordMap);
        }

        if (!keywordMap.isEmpty()) {
            int flag = 1;
            for (Map.Entry topKeyword : keywordMap.entrySet()) {
                String key = String.valueOf(topKeyword.getKey());
                if (key.trim().equalsIgnoreCase(CFConstants.NULL) || key.trim().equalsIgnoreCase(CFConstants.EMPTY_STRING)) {
                    key = CFConstants.HYPHEN;
                }
                if (widget.getIsChecked()) {
                    insertCellTopKeywords(headerTable, key, Element.ALIGN_LEFT, CFConstants.LEFT);
                    for (Map.Entry originalMap : originalKeywordMap.entrySet()) {
                        if (String.valueOf(topKeyword.getKey()).equalsIgnoreCase(String.valueOf(originalMap.getKey()))) {
                            String value = chart.formatNumber(String.valueOf(originalMap.getValue()));
                            if(String.valueOf(value) == null || String.valueOf(value).equalsIgnoreCase(CFConstants.NULL)){
                                value = "0";
                            }
                            insertCellTopKeywords(headerTable, value, Element.ALIGN_RIGHT, CFConstants.RIGHT);
                        }
                    }
                    
                } else {
//                    if (!key.trim().equals("null")) {
                    insertCellTopKeywords(headerTable, key, Element.ALIGN_LEFT, CFConstants.LEFT_AND_RIGHT);
//                    }
                }

                if (widget.getNoOfKeywords() == flag) {
                    break;
                }
                flag++;
            }
            if (widget.getIsChecked()) {
                insertLastCellTopKeywords(headerTable, CFConstants.LEFT);
                insertLastCellTopKeywords(headerTable, CFConstants.RIGHT);
            } else {
                insertLastCellTopKeywords(headerTable, CFConstants.LEFT_AND_RIGHT);
            }

        } else {
            PdfPCell cell = new PdfPCell(new Paragraph(CFConstants.NO_DATA_AVAILABLE));
            cell.setBorderWidthTop(0);
            cell.setBorderColor(Chart.borderColor);
            cell.setMinimumHeight(140f);
            cell.setBackgroundColor(Chart.ContentBackgroundColor);
            headerTable.addCell(cell);
        }

        return headerTable;

    }

    /**
     * Sort the Map based on the value in Ascending Order
     *
     * @param <K>
     * @param <V>
     * @param map
     * @return
     */
    @SuppressWarnings("rawtypes")
    public static <K extends Comparable, V extends Comparable> Map< K, V> sortByValuesAscendingOrder(Map< K, V> map) {
        List<Map.Entry<K, V>> entries = new LinkedList<Map.Entry<K, V>>(map.entrySet());

        Collections.sort(entries, new Comparator<Map.Entry<K, V>>() {

            @SuppressWarnings("unchecked")
            @Override
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                double o1value = Double.parseDouble(String.valueOf(o1.getValue()));
                double o2value = Double.parseDouble(String.valueOf(o2.getValue()));
                int returnValue = 0;

                if (o1value < o2value) {
                    returnValue = -1;
                } else if (o1value > o2value) {
                    returnValue = 1;
                } else if (o1value == o2value) {
                    returnValue = 0;
                }

                return returnValue;

            }
        });

        // LinkedHashMap will keep the keys in the order they are inserted
        // which is currently sorted on natural ordering
        Map<K, V> sortedMap = new LinkedHashMap<K, V>();

        for (Map.Entry<K, V> entry : entries) {

            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }

    /**
     * Sort the Map based on the value in Descending Order
     *
     * @param <K>
     * @param <V>
     * @param map
     * @return
     */
    @SuppressWarnings("rawtypes")
    public static <K extends Comparable, V extends Comparable> Map< K, V> sortByValuesDescendingOrder(Map< K, V> map) {
        List<Map.Entry<K, V>> entries = new LinkedList<Map.Entry<K, V>>(map.entrySet());

        Collections.sort(entries, new Comparator<Map.Entry<K, V>>() {

            @SuppressWarnings("unchecked")
            @Override
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                double o1value = Double.parseDouble(String.valueOf(o1.getValue()));
                double o2value = Double.parseDouble(String.valueOf(o2.getValue()));

                int returnValue = 0;

                if (o1value < o2value) {
                    returnValue = 1;
                } else if (o1value > o2value) {
                    returnValue = -1;
                } else if (o1value == o2value) {
                    returnValue = 0;
                }

                return returnValue;

            }
        });

        // LinkedHashMap will keep the keys in the order they are inserted
        // which is currently sorted on natural ordering
        Map<K, V> sortedMap = new LinkedHashMap<K, V>();

        for (Map.Entry<K, V> entry : entries) {

            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }

    /*
     * This method will create top Keywords table data columns, font type and
     * alignment settings, Generating Font for all Column Data, setting width of
     * column
     */
    public PdfPTable insertCellTopKeywords(PdfPTable table, String val, int alignment, String line) {

        Font dataFont = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);
        PdfPCell cellobj = new PdfPCell(new Paragraph(val, dataFont));
        cellobj.setBackgroundColor(Chart.ContentBackgroundColor);
        cellobj.setHorizontalAlignment(alignment);
        cellobj.setBorderColor(Chart.borderColor);
        cellobj.setBorderWidthTop(0);
        cellobj.setBorderWidthBottom(0);

        if (line.equalsIgnoreCase(CFConstants.LEFT)) {
            cellobj.setBorderWidthRight(0);
        } else if (line.equalsIgnoreCase(CFConstants.RIGHT)) {
            cellobj.setBorderWidthLeft(0);
        } else if (line.equalsIgnoreCase(CFConstants.LEFT_AND_RIGHT)) {

        }
        // table.setSpacingBefore(50f);       
        table.addCell(cellobj);
        return table;
    }

    /**
     * Method to insert the Last cell for top keywords
     *
     * @param table
     * @param line
     * @return
     */
    public PdfPTable insertLastCellTopKeywords(PdfPTable table, String line) {
        PdfPCell cell = new PdfPCell(new Paragraph(CFConstants.EMPTY_STRING));
        cell.setBorderWidthTop(0);
        cell.setBorderColor(Chart.borderColor);
        cell.setBackgroundColor(Chart.ContentBackgroundColor);

        if (line.equalsIgnoreCase(CFConstants.LEFT)) {
            cell.setBorderWidthRight(0);
        } else if (line.equalsIgnoreCase(CFConstants.RIGHT)) {
            cell.setBorderWidthLeft(0);
        } else if (line.equalsIgnoreCase(CFConstants.LEFT_AND_RIGHT)) {

        }

        table.addCell(cell);
        return table;
    }

}
