package com.newsletter.pdfGenerator;

/*
 * Author : Rishabh Prashar
 * 
 * This Class will get the URL and generate JSON for different Widgets ,
 * "Calling JSONConvertor" Class to covert JSON to HighChart Compatible JSON,
 * "Calling Phantom" Server to Get Chart Images from Server,
 * "Creating PDF" For Final output  
 * 
 * */
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.json.JSONException;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfDestination;
import com.itextpdf.text.pdf.PdfOutline;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.newsletter.invokeJSONserver.JSONserverInvoker;
import com.newsletter.invokeJSONserver.HeaderLogo;
import com.newsletter.pojos.CFConstants;
import com.newsletter.pojos.JsonWorkSpace;
import com.newsletter.pojos.Widget;
import com.newsletter.templateURLGenerator.WorkspaceGenerator;
import com.newsletter.widgets.AreaGraph;
import com.newsletter.widgets.BigNumber;
import com.newsletter.widgets.Chart;
import static com.newsletter.widgets.Chart.TitleFontColor;
import com.newsletter.widgets.ColumnGraph;
import com.newsletter.widgets.Grid;
import com.newsletter.widgets.PieChart;
import com.newsletter.widgets.TopKeywords;
import org.apache.log4j.Logger;

@SuppressWarnings("deprecation")
public class DashBoardUtilityImpl implements IDashBoardUtility {

    static Logger log = Logger.getLogger(DashBoardUtilityImpl.class.getName());

    Map<String, String> themeAttribute = new HashMap<>();
    ByteArrayOutputStream baosPDF = null;
    JSONserverInvoker jsonServerInvoker = null;
    ColumnGraph columnGraph = null;
    PieChart pie = null;
    AreaGraph area = null;
    TopKeywords topKeyword = null;
    BigNumber bigNumber = null;
    Grid grid = null;

    public BaseColor pdfBGColor = WebColors.getRGBColor("#FFFFFF");

    public static JsonWorkSpace workSpaceObj = null;


    /**
     *
     * @param userID
     * @param tpageName
     * @return
     * @throws IOException
     * @throws DocumentException
     * @throws JSONException
     */
    @Override
    public ByteArrayOutputStream createPDF(String userID, String tpageName) throws IOException, DocumentException, JSONException {

        /*
         * Get Workspace object and get Tabs/Pages
         */
        try {
            workSpaceObj = WorkspaceGenerator.getWorkSpace(userID);
            if (null == workSpaceObj) {
                return null;
            }
        } catch (ParseException e1) {
            //System.out.println("Problem in Workpace Generator --->  " + e1.getMessage());
            log.debug("Problem in Workpace Generator --->  " + e1.getMessage());
        }

        /*
         *  Adding widgets in 'pagesList' page wise
         *  
         * */
        TreeMap<String, List<Widget>> pagesList = WorkspaceGenerator.generatedPDF(workSpaceObj);
        if (!tpageName.trim().equalsIgnoreCase(CFConstants.ALL)) {
            if (!pagesList.containsKey(tpageName)) {
                //System.out.println("No page found of name : " + tpageName);
                log.debug("No page found of name : " + tpageName);
                return null;
            }
        }

        /*
         * Condition for create all pages or one
         *  'pageBreak' variable is used to break loop
         * */
        Boolean pageBreak = false;
        if (!tpageName.equalsIgnoreCase(CFConstants.ALL)) {
            pageBreak = true;
        }

        /*
         * Setting page size and background color
         * 
         * */
        Rectangle pageSize = new Rectangle(PageSize.A4);
        pageSize.setBackgroundColor(pdfBGColor);
        Document pdfDocument = new Document(pageSize);
        baosPDF = new ByteArrayOutputStream();
        PdfWriter writer = PdfWriter.getInstance(pdfDocument, baosPDF);

        /*
         * Creating PDF header Logo
         * 
         * */
        HeaderLogo headerLogo = new HeaderLogo();
        HeaderLogo.TableHeader event = headerLogo.new TableHeader();
        pdfDocument.setMargins(35, 35, 110, 60);
        writer.setPageEvent(event);
        event.setHeader(workSpaceObj.getCompanyName());

        /*
         * Generating Bookmark outline 
         * 
         * */
        writer.setViewerPreferences(PdfWriter.PageModeUseOutlines);
        pdfDocument.open();
        PdfOutline root = writer.getRootOutline();
        PdfOutline tabBookmark = null;
        Image retrnImage = null;

        /*
         * Iterating over widgets page wise in case user selects 'all'
         * 
         * */
        for (Map.Entry<String, List<Widget>> entry : pagesList.entrySet()) {

            String key = entry.getKey();
            List<Widget> wls = null;

            /*
             * setting For All Pages
             * */
            if (!pageBreak) {
                wls = pagesList.get(key);
                //System.err.println("Page Names -- > " + key);
                //if (!key.equalsIgnoreCase(CFConstants.SETUP_PAGE)) {

                /*
                 * Creating Page Name header for all pages
                 * and creating Bookmark for each page
                 * 
                 * */
                BaseColor cellBorderColor = WebColors.getRGBColor("#474791");
                Font fontName = new Font(Font.FontFamily.HELVETICA, 15, Font.BOLD, Chart.ContentTextColor);
                pdfDocument.newPage();
                PdfPTable pageNameTable = new PdfPTable(1);
                pageNameTable.setWidthPercentage(100);
                PdfPCell cellName = new PdfPCell(new Paragraph(key, fontName));
                cellName.setHorizontalAlignment(Element.ALIGN_CENTER);
                cellName.setVerticalAlignment(Element.ALIGN_CENTER);
                cellName.setBackgroundColor(WebColors.getRGBColor("#6A6A9E"));
                cellName.setMinimumHeight(30f);
                cellName.setBorderColor(cellBorderColor);
                pageNameTable.addCell(cellName);
                pageNameTable.setSpacingAfter(12f);
                pdfDocument.add(pageNameTable);
                tabBookmark = new PdfOutline(root, new PdfDestination(PdfDestination.FITH, writer.getVerticalPosition(true)),
                        new Paragraph(key), true);
                //}
            } else {
                wls = pagesList.get(tpageName);
                tabBookmark = new PdfOutline(root, new PdfDestination(PdfDestination.FITH, writer.getVerticalPosition(true)),
                        new Paragraph(tpageName), true);
            }
            if (tabBookmark != null) {
                tabBookmark.setStyle(Font.BOLD);
            }

            /*
             * get 'list' of Widget-name according to predecided order
             * and sorting them ,
             * 'widgetList'is sorted List
             * 
             * */
            Collections.sort(wls);
            List<String> widgetID = setWidgetColspan(wls);
            Font maessageFont = new Font(Font.FontFamily.HELVETICA, 15, Font.BOLD, Chart.ContentTextColor);
            PdfPTable mainTable = new PdfPTable(12);
            mainTable.setWidthPercentage(100f);
            mainTable.getHeader();
            PdfPCell defaultCell = mainTable.getDefaultCell();
            
            defaultCell.setBorderWidth(0);
            if (jsonServerInvoker == null) {
                jsonServerInvoker = new JSONserverInvoker();
            }
            if (wls.size() < 1) {
                PdfPCell messageCell = new PdfPCell(new Paragraph(CFConstants.NO_WIDGETS_SUPPORTED, maessageFont));
                messageCell.setColspan(12);
                messageCell.setBorderWidth(0);
                mainTable.addCell(messageCell);
            }
            for (Widget widgt : wls) {
                String Json = jsonServerInvoker.callURL(widgt.getBuildUrl());
                String widgetType = widgt.getType();
                String hcJson;
                PdfPCell firstCell;
                PdfPCell emptCell = null;
                if (Json != null) {
                    switch (widgetType) {
                        case CFConstants.BIG_NUMBER:

                            if (bigNumber == null) {
                                bigNumber = new BigNumber();
                            }
                            firstCell = new PdfPCell(bigNumber.createBigNumber2(pdfDocument, Json, widgt.getWidgetTitle(), widgt));
                            firstCell.setBorderWidth(0);
//                            firstCell.setMinimumHeight(160f);
                            firstCell.setPadding(2f);
                            firstCell.setColspan(widgt.getSizeX());
                            mainTable.addCell(firstCell);
                            if ((emptCell = widgetColSpan(widgetID, widgt)) != null) {
                                mainTable.addCell(emptCell);
                            }
                            break;

                        case CFConstants.PIE_GRAPH:
                            if (pie == null) {
                                pie = new PieChart();
                            }

                            hcJson = pie.getPieChart(Json, widgt);
                            retrnImage = jsonServerInvoker.pingServer(hcJson);

                            if (retrnImage != null) {
                                firstCell = pie.pieImageSet(widgt, widgetID, retrnImage);
                            } else {
                                firstCell = new PdfPCell();
                                firstCell.addElement(noDataAvailable(140f,widgt));
                            }
                            firstCell.setColspan(widgt.getSizeX());
                            mainTable.addCell(firstCell);
                            if ((emptCell = widgetColSpan(widgetID, widgt)) != null) {
                                mainTable.addCell(emptCell);
                            }
                            break;

                        case CFConstants.DONUT:
                            if (pie == null) {
                                pie = new PieChart();
                            }
                            hcJson = pie.getPieChart(Json, widgt);

                            retrnImage = jsonServerInvoker.pingServer(hcJson);

                            if (retrnImage != null) {
                                firstCell = pie.donutImageSet(widgt, widgetID, retrnImage);
                            } else {
                                firstCell = new PdfPCell();firstCell.addElement(noDataAvailable(140f,widgt));
                            }
                            firstCell.setColspan(widgt.getSizeX());
                            mainTable.addCell(firstCell);
                            if ((emptCell = widgetColSpan(widgetID, widgt)) != null) {
                                mainTable.addCell(emptCell);
                            }
                            break;

                        case CFConstants.TOP_KEYWORDS:

                            if (topKeyword == null) {
                                topKeyword = new TopKeywords();
                            }

                            firstCell = new PdfPCell(topKeyword.createTopKeyword(Json, widgt));
                            firstCell.setPadding(4f);
                            firstCell.setBorderWidth(0);
                            firstCell.setMinimumHeight(185f);
                            firstCell.setColspan(widgt.getSizeX());
                            mainTable.addCell(firstCell);
                            if ((emptCell = widgetColSpan(widgetID, widgt)) != null) {
                                mainTable.addCell(emptCell);
                            }
                            break;

                        case CFConstants.GRID:
                            if (grid == null) {
                                grid = new Grid();
                            }

                            firstCell = new PdfPCell(grid.createTable(pdfDocument, Json, widgt));
                            firstCell.setPadding(4f);
                            firstCell.setBorderWidth(0);
                            firstCell.setColspan(widgt.getSizeX());
                            mainTable.addCell(firstCell);
                            if ((emptCell = widgetColSpan(widgetID, widgt)) != null) {
                                mainTable.addCell(emptCell);
                            }
                            break;

                        case CFConstants.COLUMN_GRAPH:
                            if (columnGraph == null) {
                                columnGraph = new ColumnGraph();
                            }

                            hcJson = columnGraph.getColumnGraph(Json, widgt);
                            retrnImage = jsonServerInvoker.pingServer(hcJson);

                            if (null != retrnImage) {
                                firstCell = columnGraph.columnImageSet(widgt, widgetID, retrnImage);
                            } else {
                                firstCell = new PdfPCell();firstCell.addElement(noDataAvailable(140f,widgt));
                            }
                            firstCell.setColspan(widgt.getSizeX());
                            mainTable.addCell(firstCell);
                            if ((emptCell = widgetColSpan(widgetID, widgt)) != null) {
                                mainTable.addCell(emptCell);
                            }
                            break;

                        case CFConstants.LINE_GRAPH:
                            if (area == null) {
                                area = new AreaGraph();
                            }
                            hcJson = area.convertToAreaChart(Json, widgt);

                            // Ping Server for Chart Image
                            retrnImage = jsonServerInvoker.pingServer(hcJson);
                            if (null != retrnImage) {
                                firstCell = area.lineImageSet(widgt, widgetID, retrnImage);
                            } else {
                                firstCell = new PdfPCell();firstCell.addElement(noDataAvailable(140f,widgt));
                            }
//                            if(widgt.getWidgetTitle().equalsIgnoreCase("DEPP")){
//                            firstCell.setRowspan(3);
//                            }
                            firstCell.setColspan(widgt.getSizeX());
                            mainTable.addCell(firstCell);
                            if ((emptCell = widgetColSpan(widgetID, widgt)) != null) {
                                mainTable.addCell(emptCell);
                            }
                            break;

                        case CFConstants.AREA_GRAPH:
                            if (area == null) {
                                area = new AreaGraph();
                            }

                            hcJson = area.convertToAreaChart(Json, widgt);

                            retrnImage = jsonServerInvoker.pingServer(hcJson);
                            if (null != retrnImage) {
                                firstCell = area.areaImageSet(widgt, widgetID, retrnImage);
                            } else {
                                firstCell = new PdfPCell();firstCell.addElement(noDataAvailable(140f,widgt));
                            }
                            firstCell.setColspan(widgt.getSizeX());
                            mainTable.addCell(firstCell);
                            if ((emptCell = widgetColSpan(widgetID, widgt)) != null) {
                                mainTable.addCell(emptCell);
                            }
                            break;

                        default:
                            PdfPCell imgCell = new PdfPCell(new Paragraph(CFConstants.WIDGET_NOT_SUPPORTED));
                            imgCell.setBorderWidth(1);
                            mainTable.addCell(imgCell);
                            break;
                    }
                } else {
                    firstCell= new PdfPCell();
                    firstCell.setBorderWidth(0);
                    firstCell.addElement(noDataAvailable(150f,widgt));
                    firstCell.setColspan(widgt.getSizeX());
                    mainTable.addCell(firstCell);
                    if ((emptCell = widgetColSpan(widgetID, widgt)) != null) {
                        mainTable.addCell(emptCell);
                    }

                }
            }
            pdfDocument.add(mainTable);
            if (pageBreak) {
                break;
            }
        }
        pdfDocument.close();
        return baosPDF;
    }

    /**
     *
     * @param wls
     * @return
     */
    public List<String> setWidgetColspan(List<Widget> wls) {

        int i = 0;
        int totalCols = 0;
        String lastW = "";
        int lastrow = -1;
        List<String> lastwidgetinrow = new ArrayList<String>();

        for (i = 0; i < wls.size(); i++) {

            if (lastrow != wls.get(i).getRow()) {
//				if(lastW.trim().equals("")){
//					lastW=wls.get(i).getWidgetName();
//				}
                lastwidgetinrow.add(lastW + CFConstants.HYPHEN + totalCols);
                totalCols = 0;
                lastrow = -1;
                lastW = "";
            }

            lastrow = wls.get(i).getRow();
            lastW = wls.get(i).getWidgetName();
            totalCols += wls.get(i).getSizeX();
        }
        totalCols = 0;
        for (i = 0; i < wls.size(); i++) {
            if (wls.get(wls.size() - 1).getRow() == wls.get(i).getRow()) {
                totalCols += wls.get(i).getSizeX();
                lastW = wls.get(i).getWidgetName();
            }
        }
        lastwidgetinrow.add(lastW + CFConstants.HYPHEN + totalCols);
        lastwidgetinrow.remove(0);
        //System.out.println(lastwidgetinrow);

        return lastwidgetinrow;
    }

    public PdfPCell widgetColSpan(List<String> widgetID, Widget widget) {
        int newColspan = 0;
        PdfPCell cell = new PdfPCell(new Paragraph(" "));
        for (String id : widgetID) {
            if (widget.getSizeX() != 12) {
                if ((id.substring(0, id.lastIndexOf(CFConstants.HYPHEN)).equalsIgnoreCase(widget.getWidgetName()))) {
                    int colspan = Integer.parseInt(id.substring(id.lastIndexOf(CFConstants.HYPHEN) + 1));
//                    newColspan = widget.getSizeX() + (12 - colspan);
                    newColspan = (12 - colspan);
//                    firstCell.setColspan(newColspan);
                }
            }
        }
        cell.setColspan(newColspan);
        cell.setBorderWidth(0);
        if (newColspan == 0) {
            cell = null;
        }

        return cell;
    }

    /**
     *
     * @param minHeight
     * @param widgt
     * @return
     */
    public PdfPTable noDataAvailable(float minHeight, Widget widgt) {
        PdfPTable tb = new PdfPTable(1);
        tb.setWidthPercentage(100f);
        String headerTitle = widgt.getWidgetTitle().trim();
        Font font = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL, TitleFontColor);
        if (headerTitle.trim().equals(CFConstants.EMPTY_STRING)) {
            headerTitle = CFConstants.SPACE;
        }
        PdfPCell headerCell = new PdfPCell(new Paragraph(headerTitle, font));
        headerCell.setBackgroundColor(Chart.HeaderBackgroundColor);
        PdfPCell cell = new PdfPCell(new Paragraph(CFConstants.NO_DATA_AVAILABLE));
        cell.setBorderWidthTop(0);
        cell.setBorderColor(Chart.borderColor);
        cell.setMinimumHeight(minHeight);
        cell.setBackgroundColor(Chart.ContentBackgroundColor);
        tb.addCell(headerCell);
        tb.addCell(cell);
        return tb;
    }

}
