package com.newsletter.pdfGenerator;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.json.JSONException;
import com.itextpdf.text.DocumentException;

public interface IDashBoardUtility {
	
	public ByteArrayOutputStream createPDF(String userID,String tpageName) throws IOException, DocumentException, JSONException;
	
	//public Image createImageDataPDF(Document pdfDocument,Image image,String title);
	
	//public PdfPTable createTable(Document document,String jsonString,String title, Widget widget) throws JSONException, FileNotFoundException, DocumentException;
	
	//public PdfPTable insertTableHeader(PdfPTable table,String val);
	
	//public PdfPTable insertCell(PdfPTable table,String val);
	
	//public int getTableColumns(JSONObject jsnObj) throws JSONException;
	
	//public PdfPTable createBigNumber(Document document,String jsonString,String title, Widget widget) throws JSONException, DocumentException;
	
	//public PdfPTable insertCellTopKeywords(PdfPTable table, String val, int alignment);
        
        //public PdfPTable insertLastCellTopKeywords(PdfPTable table);
		
}
