/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.newsletter.pojos;

import java.util.ResourceBundle;

/**
 *
 * Author : Ratan
 *
 * This class contains all the constants which are used Main class to get JSON
 * object and to build URL
 */
public class CFConstants {

    public final static String APP_URL = "http://sandbox.clickfuel.com:8080/";
    public final static String WORKSPACE_URL_STR = "api/v1/workspace?";
    public final static String DATA_URL_STR = "api/v1/data?";
    public final static String START_DATE = "startDate";
    public final static String END_DATE = "endDate";
    public final static String USER_ID = "userId";
    public final static String COMPANY_ID = "companyId";
    public final static String COMPANY_NAME = "companyName";
    public final static String PAGES = "pages";
    public final static String PAGE_NAME = "displayName";
    public final static String WIDGETS = "widgets";
    public final static String DEFAULT_VALUE = "defaultValue";
    public final static String DEFAULT_DATE = "defaultDate";
    public final static String WIDGET_TYPE = "type";
    public final static String WIDGET_WIDGET_TYPE = "widgetType";
    public final static String WIDGET_API_FIELDS = "apiFields";
    public final static String WIDGET_FIELDS = "fields";
    public final static String WIDGET_SUMMARY_BY = "summaryBy";
    public final static String WIDGET_API_SUMMARY_BY = "apiSummaryBy";
    public final static String WIDGET_FEED_METADATA_ID = "feedMetadataId";
    public final static String WIDGET_METADATA_ID = "metadataId";
    public final static String WIDGET_CAMPAIGN_GROUPID = "campaignGroupId";
    public final static String WIDGET_API_SUM = "apiSum";
    public final static String WIDGET_SUM = "sum";
    public final static String WIDGET_API_GROUPBY = "apiGroupBy";
    public final static String WIDGET_GROUP = "group";
    public final static String WIDGET_SIZE_X = "sizeX";
    public final static String WIDGET_SIZE_Y = "sizeY";
    public final static String WIDGET_METRICS = "metrics";
    public final static String WIDGET_FORMAT = "format";
    public final static String TOP_KEYWORDS = "top keywords";
    public final static String GRID = "grid";
    public final static String AREA = "area";
    public final static String DAY = "day";
    public final static String LINE_GRAPH = "line graph";
    public final static String DATE_FORMAT = "yyyyMMdd";
    public final static String US_DATE_FORMAT = "MM-dd-yyyy hh:mm aa";
    public final static String CREATED_ON = "Created on";
    public final static String LAST_7_DAYS = "LAST 7 DAYS";
    public final static String LAST_30_DAYS = "LAST 30 DAYS";
    public final static String LAST_MONTH = "LAST MONTH";
    public final static String TODAY = "TODAY";
    public final static String YESTERDAY = "YESTERDAY";
    public final static String THIS_MONTH = "THIS MONTH";
    public final static String AMPERSAND = "&";
    public final static String EQUALS = "=";
    public final static String NULL = "null";
    public final static String COMMA = ",";
    public final static String COLON = ":";
    public final static String DOT = ".";
    public final static String HYPHEN = "-";
    static public final String EMPTY_STRING = "";
    static public final String SPACE = " ";
    public final static String WIDGET_TITLE = "title";
    public final static String WIDGET_PRE_FILTER_CONDITION = "preFilterByOption";
    public final static String WIDGET_FILTER_CONDITION = "filterCondition";
    public final static String WIDGET_FILTER_BY = "filterBy";
    public final static String WIDGET_PRE_FILTER_BY_VALUE = "preFilterByValue";
    public final static String WIDGET_FILTER_BY_VALUE = "filterByValue";
    static public final String ROW = "row";
    public final static String COL = "col";
    static public final String IN_ROW = "inRow";
    public final static String IN_COL = "inCol";
    public final static String API_TOKEN = "apiToken";
    public final static String DUMMY_TOKEN = "token";
    public final static String LOGO_PATH = "/app/img/";
    public final static String LOGO_NAME = "/logo";
    public final static String USER_AGENT = "Mozilla/5.0";
    public final static String PHANTOMJS_URL = "http://172.31.145.19:8080/highcharts-export-web/";
    public final static String OPTIONS = "options";
    public final static String TOPKEYWORDS_NOOFKEYWORDS = "noOfKeywords";
    public final static String TOPKEYWORDS_SORTBY = "sortBy";
    public final static String TOPKEYWORDS_HIGHEST_TO_LOWEST = "Highest to Lowest";
    public final static String TOPKEYWORDS_LOWEST_TO_HIGHEST = "Lowest to Highest";
    public final static String WIDGET_STYLES = "styles";
    public final static String BIGNUMBER_HEADERTEXTSIZE = "bigNumber_headerTextSize";
    public final static String BIGNUMBER_HEADERTEXTCOLOR = "bigNumber_headerTextColor";    
    public final static String BIGNUMBER_NUMBERTEXTSIZE = "bigNumber_numberTextSize";
    public final static String BIGNUMBER_NUMBERTEXTCOLOR = "bigNumber_numberTextColor";
    public final static String COLORS = "colors";
    public final static String WIDGET_FILTERBY_COLUMN = "preFilterByColumn";
    public final static String DONUT = "donut chart";
    public final static String WIDGET_ISDRAWN = "isDrawn";
    public final static String DEFAULT_COMPANYNAME = "AutoTest";
    public static ResourceBundle BUNDLE = ResourceBundle.getBundle("properties/config");
    public static ResourceBundle WIDGET_BUNDLE = ResourceBundle.getBundle("properties/widget");
    public static ResourceBundle THEME_BUNDLE = ResourceBundle.getBundle("properties/default_theme");
    public final static String  CUSTOM_RANGE="Custom Range";
    
    public final static String AREA_GRAPH = "area graph";
    public final static String COLUMN_GRAPH = "column graph";
    public final static String COLUMN = "column";
    public final static String WIDGET_CHARTOPTIONS = "chartOptions";
    public final static String WIDGET_PLOTOPTIONS = "plotOptions";
    public final static String WIDGET_CHART = "chart";
    public final static String WIDGET_BGCOLOR = "backgroundColor";
    public final static String WIDGET_OPTIONS3D = "options3d";
    public final static String TOPKEYWORDS_ISCHECKED = "isChecked";
    
    public final static String AREA_GRAPH_STACKED = "Stacked Area Graph";
    public final static String LINE_INVERTED_AXIS = "Inverted axis";
    public final static String PIE_3D = "3d Pie Chart";
    public final static String DONUT_3D = "3d Donut Chart";
    public final static String PIE_CHART = "Pie Chart";
    public final static String WIDGET_NOT_SUPPORTED = "Widget Not Supported !! ";
    public final static String NO_WIDGETS_SUPPORTED = "This page doesn't have any widgets that are currently supported !!";
    //public final static String DONUT_CHART = "Donut Chart";    
    //public final static String PIE_GRAPH = "Pie Chart";
    public final static String DONUT_GRAPH = "Donut Graph";
    public final static String PIE = "pie";

    
    public final static String ALL = "all";
    public final static String SETUP_PAGE = "SetUp Page";
    public final static String BIG_NUMBER = "big number";
    public final static String PIE_GRAPH = "pie graph";
    public final static String NO_DATA_AVAILABLE = " No Data Available !!";
    public final static String GRID_NO_DATA = "No Data";
    public final static String BARCHART_GRAPH = "Bar Chart";
    public final static String LINE_CHART = "Line Chart";

    public final static String LEFT = "LEFT";
    public final static String RIGHT = "RIGHT";
    public final static String LEFT_AND_RIGHT = "LEFT_AND_RIGHT";

    
     public final static String WIDGET_PAGESIZE = "pagesize";
     public final static String GRID_SIZE = "size";
     
    
    /*
     * Constants For Charts
     * */    
    public final static String CHART_DATA = "data";
    public final static String CHART_NAME = "name";
    public final static String CHART_METRIC = "metric";
    public final static String CHART_SERIES = "series";
    public final static String CHART_CATEGORY = "categories";
    public final static String CHART_COMMA = ",";   
}
