/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.newsletter.pojos;

import java.util.Date;

/**
 *
 * @author Ratan
 */
public class Widget implements Comparable<Widget> {

    private String metadataId;
    private String startDate;
    private String endDate;
    private String group;
    private String fields;
    private String sum;
    private String summaryBy;
    private String type;
    private String widgetName;
    private String metrics;
    private int sizeX;
    private int sizeY;
    private String buildUrl;
    private String widgetTitle;
    private int noOfKeywords;
    private String sortBy;
    private Boolean isChecked;
    private int row;
    private int col;
    private String defaultDate;
    private String filterCondition;
    private String filterBy;
    private String filterByValue;
    private int bigNumberHeaderTextSize;
    private String bigNumberHeaderTextColor;
    private int bigNumberNumberTextSize;
    private String bigNumberNumberTextColor;
    private String widgetType;
    private String campaignGroupId;
    private String options3d;
    private String colors;

    private String widgetID;
    private Boolean isCampaignGroup;
    private Boolean showOptions;
    private String template;
    private String controller;
    private Boolean enablescheduler;
    private Boolean isNewUI;
    private Boolean widgetLoading;
    private Boolean isDrawn;
    private Boolean tabId;

    private String widgetSubtype;
    private String widgetBGColor;
    private int widgetGridSize;

    public Widget() {
        row = -1;
        col = -1;
    }

    public String getWidgetID() {
        return widgetID;
    }

    public void setWidgetID(String widgetID) {
        this.widgetID = widgetID;
    }

    public String getCampaignGroupId() {
        return campaignGroupId;
    }

    public void setCampaignGroupId(String campaignGroupId) {
        this.campaignGroupId = campaignGroupId;
    }

    public Boolean getIsCampaignGroup() {
        return isCampaignGroup;
    }

    public void setIsCampaignGroup(Boolean isCampaignGroup) {
        this.isCampaignGroup = isCampaignGroup;
    }

    public Boolean getShowOptions() {
        return showOptions;
    }

    public void setShowOptions(Boolean showOptions) {
        this.showOptions = showOptions;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getController() {
        return controller;
    }

    public void setController(String controller) {
        this.controller = controller;
    }

    public Boolean getEnablescheduler() {
        return enablescheduler;
    }

    public void setEnablescheduler(Boolean enablescheduler) {
        this.enablescheduler = enablescheduler;
    }

    public Boolean getIsNewUI() {
        return isNewUI;
    }

    public void setIsNewUI(Boolean isNewUI) {
        this.isNewUI = isNewUI;
    }

    public Boolean getWidgetLoading() {
        return widgetLoading;
    }

    public void setWidgetLoading(Boolean widgetLoading) {
        this.widgetLoading = widgetLoading;
    }

    public Boolean getIsDrawn() {
        return isDrawn;
    }

    public void setIsDrawn(Boolean isDrawn) {
        this.isDrawn = isDrawn;
    }

    public Boolean getTabId() {
        return tabId;
    }

    public void setTabId(Boolean tabId) {
        this.tabId = tabId;
    }

    @Override
    public int compareTo(Widget o) {

        String coord1 = String.valueOf(Integer.toString(this.row) + Integer.toString(this.col));
        String coord2 = String.valueOf(Integer.toString(o.row) + Integer.toString(o.col));
        int cood1 = Integer.parseInt(coord1);
        int cood2 = Integer.parseInt(coord2);
        if (cood1 > cood2) {
            return 1;
        } else if (cood1 < cood2) {
            return -1;
        } else {
            return 0;
        }
    }

    public String getWidgetTitle() {
        return widgetTitle;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public void setWidgetTitle(String widgetTitle) {
        this.widgetTitle = widgetTitle;
    }

    public String getBuildUrl() {
        return buildUrl;
    }

    public void setBuildUrl(String buildUrl) {
        this.buildUrl = buildUrl;
    }

    public int getSizeX() {
        return sizeX;
    }

    public void setSizeX(int sizeX) {
        this.sizeX = sizeX;
    }

    public int getSizeY() {
        return sizeY;
    }

    public void setSizeY(int sizeY) {
        this.sizeY = sizeY;
    }

    public String getMetadataId() {
        return metadataId;
    }

    public void setMetadataId(String metadataId) {
        this.metadataId = metadataId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getFields() {
        return fields;
    }

    public void setFields(String fields) {
        this.fields = fields;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }

    public String getSummaryBy() {
        return summaryBy;
    }

    public void setSummaryBy(String summaryBy) {
        this.summaryBy = summaryBy;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWidgetName() {
        return widgetName;
    }

    public void setWidgetName(String widgetName) {
        this.widgetName = widgetName;
    }

    public String getMetrics() {
        return metrics;
    }

    public void setMetrics(String metrics) {
        this.metrics = metrics;
    }

    public int getNoOfKeywords() {
        return noOfKeywords;
    }

    public void setNoOfKeywords(int noOfKeywords) {
        this.noOfKeywords = noOfKeywords;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getDefaultDate() {
        return defaultDate;
    }

    public void setDefaultDate(String defaultDate) {
        this.defaultDate = defaultDate;
    }

    public String getFilterCondition() {
        return filterCondition;
    }

    public void setFilterCondition(String filterCondition) {
        this.filterCondition = filterCondition;
    }

    public String getFilterBy() {
        return filterBy;
    }

    public void setFilterBy(String filterBy) {
        this.filterBy = filterBy;
    }

    public String getFilterByValue() {
        return filterByValue;
    }

    public void setFilterByValue(String filterByValue) {
        this.filterByValue = filterByValue;
    }

    public int getBigNumberHeaderTextSize() {
        return bigNumberHeaderTextSize;
    }

    public void setBigNumberHeaderTextSize(int bigNumberHeaderTextSize) {
        this.bigNumberHeaderTextSize = bigNumberHeaderTextSize;
    }

    public String getBigNumberHeaderTextColor() {
        return bigNumberHeaderTextColor;
    }

    public void setBigNumberHeaderTextColor(String bigNumberHeaderTextColor) {
        this.bigNumberHeaderTextColor = bigNumberHeaderTextColor;
    }

    public int getBigNumberNumberTextSize() {
        return bigNumberNumberTextSize;
    }

    public void setBigNumberNumberTextSize(int bigNumberNumberTextSize) {
        this.bigNumberNumberTextSize = bigNumberNumberTextSize;
    }

    public String getBigNumberNumberTextColor() {
        return bigNumberNumberTextColor;
    }

    public void setBigNumberNumberTextColor(String bigNumberNumberTextColor) {
        this.bigNumberNumberTextColor = bigNumberNumberTextColor;
    }

    public String getWidgetType() {
        return widgetType;
    }

    public void setWidgetType(String widgetType) {
        this.widgetType = widgetType;
    }

    public String getWidgetSubtype() {
        return widgetSubtype;
    }

    public void setWidgetSubtype(String widgetSubtype) {
        this.widgetSubtype = widgetSubtype;
    }

    public String getWidgetBGColor() {
        return widgetBGColor;
    }

    public void setWidgetBGColor(String widgetBGColor) {
        this.widgetBGColor = widgetBGColor;
    }

    public String getOptions3d() {
        return options3d;
    }

    public void setOptions3d(String options3d) {
        this.options3d = options3d;
    }

    public int getWidgetGridSize() {
        return widgetGridSize;
    }

    public void setWidgetGridSize(int widgetGridSize) {
        this.widgetGridSize = widgetGridSize;
    }

    public Boolean getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(Boolean isChecked) {
        this.isChecked = isChecked;
    }

    public String getColors() {
        return colors;
    }

    public void setColors(String colors) {
        this.colors = colors;
    }

}
