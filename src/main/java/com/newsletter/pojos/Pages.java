/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.newsletter.pojos;

import java.util.List;

/**
 *
 * @author Ratan
 */
public class Pages {

    private int pageSize;
    private int totalPages;
    private int currentPageNumber;
    private int pageOrder;
    private int displayName;
    private String pageName;
    private List<Widget> widget;

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getCurrentPageNumber() {
        return currentPageNumber;
    }

    public void setCurrentPageNumber(int currentPageNumber) {
        this.currentPageNumber = currentPageNumber;
    }

    public int getPageOrder() {
        return pageOrder;
    }

    public void setPageOrder(int pageOrder) {
        this.pageOrder = pageOrder;
    }

    public int getDisplayName() {
        return displayName;
    }

    public void setDisplayName(int displayName) {
        this.displayName = displayName;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public List<Widget> getWidget() {
        return widget;
    }

    public void setWidget(List<Widget> widget) {
        this.widget = widget;
    }

}
