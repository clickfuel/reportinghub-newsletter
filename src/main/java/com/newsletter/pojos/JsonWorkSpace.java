/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.newsletter.pojos;

import java.util.List;

/**
 *
 * @author Ratan
 */
public class JsonWorkSpace {

    private int userID;
    private List<Pages> page;
    private String apiDataCallURL;
    private String companyID;
    private String companyName;

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public List<Pages> getPage() {
        return page;
    }

    public void setPage(List<Pages> page) {
        this.page = page;
    }

    public String getApiDataCallURL() {
        return apiDataCallURL;
    }

    public void setApiDataCallURL(String apiDataCallURL) {
        this.apiDataCallURL = apiDataCallURL;
    }

    public String getCompanyID() {
        return companyID;
    }

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

}
