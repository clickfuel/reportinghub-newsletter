package com.newsletter.templateURLGenerator;
/*
 * Author : Ratan Kumar
 * 
 * This Class is to get the builded URL for a particular widget
 * 
 * */

import com.newsletter.pojos.CFConstants;
import com.newsletter.pojos.Widget;
import org.apache.log4j.Logger;

public class URLGenerator {

    static Logger log = Logger.getLogger(URLGenerator.class.getName());

    /**
     * Main Method to Buid the URL needed to fetch the data for the widgets
     *
     * @param widgetObj
     * @param companyID
     * @param pagename
     * @return
     */
    public static String getWidgetUrl(Widget widgetObj, String companyID, String pagename) {

        StringBuilder buildUrl = new StringBuilder();

        String URL = CFConstants.BUNDLE.getString("URL");  // Fetch content data from given URL
        String URL_PORT = CFConstants.BUNDLE.getString("URL_PORT");
        String APP_URL = URL + CFConstants.COLON + URL_PORT + "/";
        buildUrl.append(APP_URL).append(CFConstants.DATA_URL_STR);

        String widgetType = widgetObj.getType();
        String filterCondition = widgetObj.getFilterCondition();
        String filterBy = widgetObj.getFilterBy();
        String filterByValue = widgetObj.getFilterByValue();

        //Get the general URL for the widget URL
        buildUrl.append(getGenURL(widgetObj, companyID));

        //Add filter Conditions to the widget URL
        buildUrl.append(getFilters(filterCondition, filterBy, filterByValue));

        //GRID and DONUT should not contain Metrics (Sum)
        /*if (widgetObj.getType() != null && !widgetType.equalsIgnoreCase(CFConstants.GRID) && !widgetType.equalsIgnoreCase(CFConstants.DONUT)) {
         buildUrl.append(getSumMetrics(widgetObj.getSum(), widgetObj.getMetrics()));
         }*/
        // DONUT should not contain Sum parameter
//        if (widgetObj.getType() != null && !widgetType.equalsIgnoreCase(CFConstants.DONUT)) {
//            //buildUrl.append(getSumMetrics(widgetObj.getSum(), widgetObj.getMetrics()));
//            buildUrl.append(buildURLParams(CFConstants.WIDGET_SUM, widgetObj.getSum()));
//        }
        switch (widgetType) {

            case CFConstants.GRID:
                buildUrl.append(buildURLParams(CFConstants.WIDGET_FORMAT, widgetType));
                buildUrl.append(buildURLParams(CFConstants.WIDGET_FIELDS, widgetObj.getFields()));
                buildUrl.append(buildURLParams(CFConstants.WIDGET_SUMMARY_BY, widgetObj.getSummaryBy()));
                break;

            case CFConstants.DONUT:
                buildUrl.append(buildURLParams(CFConstants.WIDGET_FIELDS, widgetObj.getFields()));
                if (widgetObj.getSum() != null || !String.valueOf(widgetObj.getSum()).equalsIgnoreCase(CFConstants.NULL)) {
                    buildUrl.append(buildURLParams(CFConstants.WIDGET_SUM, widgetObj.getSum()));
                }
                break;

            case CFConstants.PIE_GRAPH:
                if (widgetObj.getFields() != null || !String.valueOf(widgetObj.getFields()).equalsIgnoreCase(CFConstants.NULL)) {
                    buildUrl.append(buildURLParams(CFConstants.WIDGET_FIELDS, widgetObj.getFields()));
                }
                if (widgetObj.getSum() != null || !String.valueOf(widgetObj.getSum()).equalsIgnoreCase(CFConstants.NULL)) {
                    buildUrl.append(buildURLParams(CFConstants.WIDGET_SUM, widgetObj.getSum()));
                }
                break;

            case CFConstants.AREA_GRAPH:
            case CFConstants.LINE_GRAPH:
                buildUrl.append(buildURLParams(CFConstants.WIDGET_FORMAT, CFConstants.AREA));
                buildUrl.append(buildURLParams(CFConstants.WIDGET_SUMMARY_BY, widgetObj.getSummaryBy()));
                buildUrl.append(buildURLParams(CFConstants.WIDGET_SUM, widgetObj.getSum()));
                break;

            case CFConstants.COLUMN_GRAPH:
                buildUrl.append(buildURLParams(CFConstants.WIDGET_FORMAT, CFConstants.COLUMN));
                buildUrl.append(buildURLParams(CFConstants.WIDGET_SUMMARY_BY, CFConstants.DAY));
                buildUrl.append(buildURLParams(CFConstants.WIDGET_SUM, widgetObj.getSum()));
                break;

            case CFConstants.TOP_KEYWORDS:
                buildUrl.append(buildURLParams(CFConstants.WIDGET_FORMAT, CFConstants.GRID));
                buildUrl.append(buildURLParams(CFConstants.WIDGET_SUM, widgetObj.getSum()));
                break;

            case CFConstants.BIG_NUMBER:

                buildUrl.append(buildURLParams(CFConstants.WIDGET_SUM, widgetObj.getSum()));

                buildUrl.append(buildURLParams(CFConstants.WIDGET_FORMAT, CFConstants.GRID));

                break;

            default:

                break;

        }
        buildUrl.deleteCharAt(buildUrl.lastIndexOf(CFConstants.AMPERSAND));
        log.debug("WIDGET URLS - - " + widgetType + " - " + widgetObj.getWidgetTitle() + " - " + pagename + " - " + String.valueOf(buildUrl));
        return String.valueOf(buildUrl);
    }

    /**
     * Main Method to get the Common URL Parameters for the widgets
     *
     * @param widgetObj
     * @param companyID
     * @return
     */
    public static String getGenURL(Widget widgetObj, String companyID) {
        StringBuilder genURL = new StringBuilder();
        genURL.append(buildURLParams(CFConstants.DUMMY_TOKEN, String.valueOf(System.currentTimeMillis())));
        genURL.append(buildURLParams(CFConstants.COMPANY_ID, companyID));

        if (widgetObj.getMetadataId() != null && !widgetObj.getMetadataId().equalsIgnoreCase(CFConstants.NULL)) {
            genURL.append(buildURLParams(CFConstants.WIDGET_METADATA_ID, widgetObj.getMetadataId()));
        } else if (widgetObj.getCampaignGroupId() != null && !widgetObj.getCampaignGroupId().equalsIgnoreCase(CFConstants.NULL)) {
            genURL.append(buildURLParams(CFConstants.WIDGET_CAMPAIGN_GROUPID, widgetObj.getCampaignGroupId()));
        }

        genURL.append(buildURLParams(CFConstants.START_DATE, widgetObj.getStartDate()));
        genURL.append(buildURLParams(CFConstants.END_DATE, widgetObj.getEndDate()));

        genURL.append(buildURLParams(CFConstants.WIDGET_GROUP, widgetObj.getGroup()));

        return String.valueOf(genURL);
    }

    /**
     * Main Method to get the filter Parameters to build URL
     *
     * @param filterCondition
     * @param filterBy
     * @param filterByValue
     * @return
     */
    public static String getFilters(String filterCondition, String filterBy, String filterByValue) {
        StringBuilder buildFilters = new StringBuilder();
        //Add the filter Conditions for the widgets

        buildFilters.append(buildURLParams(CFConstants.WIDGET_FILTER_CONDITION, filterCondition));

        buildFilters.append(buildURLParams(CFConstants.WIDGET_FILTER_BY, filterBy));

        buildFilters.append(buildURLParams(CFConstants.WIDGET_FILTER_BY_VALUE, filterByValue));

        return String.valueOf(buildFilters);
    }

    /**
     * Method to append the EQUALS and AMPERSAND as in the URL to fetch data
     *
     * @param param
     * @param value
     * @return
     */
    public static String buildURLParams(String param, String value) {

        if (value != null && !value.equalsIgnoreCase(CFConstants.NULL)) {
            StringBuilder buildParams = new StringBuilder();
            buildParams.append(param).append(CFConstants.EQUALS).append(value).append(CFConstants.AMPERSAND);

            return String.valueOf(buildParams);
        } else {
            return CFConstants.EMPTY_STRING;
        }
    }
}
