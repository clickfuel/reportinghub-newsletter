/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.newsletter.templateURLGenerator;

/**
 * Author : Ratan
 *
 * This class is used to get the Entire JSONObject This class contains the
 * methods To get the Json content by passing the API data call URL
 * (readJsonFromUrl) To get the Entire JSON Workspace as Object
 * (GenerateJSONWorkSpace) To get the URLs for the different widgets
 * (generatedURLfromJsonObj)
 *
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.newsletter.pojos.CFConstants;
import com.newsletter.pojos.JsonWorkSpace;
import com.newsletter.pojos.Pages;
import com.newsletter.pojos.Widget;

public class WorkspaceGenerator {

    /**
     * Method to read Json content from URL and return content as String
     *
     * @param url
     * @return
     * @throws IOException
     */
    public String readJsonFromUrl(String url) throws IOException {
        URL website = new URL(url);
        URLConnection connection = website.openConnection();
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(
                        connection.getInputStream()));

        StringBuilder builder = new StringBuilder();
        String inputLine;

        while ((inputLine = bufferedReader.readLine()) != null) {
            builder.append(inputLine);
        }

        bufferedReader.close();

        return String.valueOf(builder);
    }

    /**
     * Main method to perform required operation after fetching data from other
     * methods
     *
     * @param UserID
     * @return
     * @throws IOException
     * @throws ParseException
     * @throws JSONException
     */
    public static JsonWorkSpace getWorkSpace(String UserID) throws IOException, ParseException, JSONException {

        WorkspaceGenerator jfRead = new WorkspaceGenerator();
        String URL = CFConstants.BUNDLE.getString("URL");  // Fetch content data from given URL
        String URL_PORT = CFConstants.BUNDLE.getString("URL_PORT");
        String APP_URL = URL + CFConstants.COLON + URL_PORT + "/";
        String urlSB = APP_URL + CFConstants.WORKSPACE_URL_STR + CFConstants.USER_ID + CFConstants.EQUALS + UserID;

        String stringSB = jfRead.readJsonFromUrl(urlSB);

        JsonWorkSpace generatedJsonObj = jfRead.GenerateUrl(stringSB);

        return generatedJsonObj;
    }

    /**
     * Method to fetch entire Workspace as JSONObject
     *
     * @param workspaceJsonString
     * @return
     * @throws ParseException
     * @throws JSONException
     */
    public JsonWorkSpace GenerateUrl(String workspaceJsonString) throws ParseException, JSONException {
        WorkspaceGenerator jsonFileRead = new WorkspaceGenerator();
        JsonWorkSpace workSpaceObj = new JsonWorkSpace();
//  ERROR For USER 20
        // workspaceJsonString.replaceAll("\\\\n", "");
        //workspaceJsonString.replaceAll("\\\\", "");
        if (workspaceJsonString.contains("\\n")) {
            return null;
        }
        JSONObject jsonObject = new JSONObject(workspaceJsonString);
        String companyId = (String) jsonObject.get(CFConstants.COMPANY_ID);
        String companyName = (String) jsonObject.get(CFConstants.COMPANY_NAME);
        workSpaceObj.setCompanyID(companyId);
        workSpaceObj.setCompanyName(companyName);

        JSONObject jsonObjectPages = (JSONObject) jsonObject.get(CFConstants.PAGES);

        List<Pages> pagesList = new ArrayList<Pages>();
        @SuppressWarnings("unchecked")
        Iterator<String> iteratePages = jsonObjectPages.keys();
        while (iteratePages.hasNext()) {

            JSONObject jsonObjectInsidePages = (JSONObject) jsonObjectPages.get(iteratePages.next());

            String pageNamefromJson = (String) jsonObjectInsidePages.get(CFConstants.PAGE_NAME);
            Pages pages = new Pages();
            pages.setPageName(pageNamefromJson);

            @SuppressWarnings("unchecked")
            Iterator<String> keysInsidePages = jsonObjectInsidePages.keys();
            while (keysInsidePages.hasNext()) {
                String widgets = (String) keysInsidePages.next();

                if (widgets.equalsIgnoreCase(CFConstants.WIDGETS)) {

                    String widgetObject = String.valueOf(jsonObjectInsidePages.get(widgets));
                    JSONObject insideWidget = new JSONObject(widgetObject);

                    JSONArray widgetArray = insideWidget.names();

                    if (widgetArray != null) {

                        List<Widget> widgetList = new ArrayList<Widget>();

                        // for loop to get all widget names
                        for (int currentWidget = 0; currentWidget < widgetArray.length(); currentWidget++) {
                            String inRow = null;
                            String inCol = null;
                            String metrics = null;

                            Widget widget = new Widget();

                            String widgetName = String.valueOf(widgetArray.get(currentWidget));
                            widget.setWidgetName(widgetName);

                            JSONObject jsonObjectWidgets = (JSONObject) insideWidget.get(widgetName);
                            @SuppressWarnings("unchecked")
                            Iterator<String> iterateWidgetkeys = jsonObjectWidgets.keys();

                            while (iterateWidgetkeys.hasNext()) {

                                //Perform Data Loading or Use Map Object and Call data urls asyn
                                String widgetParameter = (String) iterateWidgetkeys.next();

                                switch (widgetParameter) {

                                    case CFConstants.DEFAULT_VALUE:
                                    case CFConstants.DEFAULT_DATE:

                                        String defValue = jsonObjectWidgets.getString(widgetParameter);
                                        if (defValue.equalsIgnoreCase(CFConstants.CUSTOM_RANGE)) {
                                            Iterator<?> innerKeys = jsonObjectWidgets.keys();
                                            boolean customDate = false;
                                            while (innerKeys.hasNext()) {
                                                if (String.valueOf(innerKeys.next()).equalsIgnoreCase("startDate")) {
                                                    customDate = true;
                                                }
                                            }
                                            if (customDate) {
                                                SimpleDateFormat frmt = new SimpleDateFormat("yyyy-MM-dd");
                                                Date stDate = frmt.parse(jsonObjectWidgets.getString("startDate").substring(0, 10));
                                                Date endDate = frmt.parse(jsonObjectWidgets.getString("endDate").substring(0, 10));
                                                String formatedsDate = new SimpleDateFormat(CFConstants.DATE_FORMAT).format(stDate);
                                                String formatedeDate = new SimpleDateFormat(CFConstants.DATE_FORMAT).format(endDate);
                                                widget.setStartDate(formatedsDate);
                                                widget.setEndDate(formatedeDate);
                                            }
                                        } else {

                                            widget.setDefaultDate(defValue);
                                            Map<String, String> Dates = jsonFileRead.DateFormat(defValue);
                                            widget.setStartDate(Dates.get(CFConstants.START_DATE));
                                            widget.setEndDate(Dates.get(CFConstants.END_DATE));
                                        }
                                        break;

                                    case CFConstants.WIDGET_TYPE:
                                        widget.setType(jsonObjectWidgets.getString(widgetParameter).toLowerCase());
                                        break;

                                    case CFConstants.WIDGET_WIDGET_TYPE:
                                        widget.setWidgetType(jsonObjectWidgets.getString(widgetParameter));
                                        break;

                                    case CFConstants.WIDGET_API_FIELDS:

                                        widget.setFields(String.valueOf(jsonObjectWidgets.get(widgetParameter)));
                                        break;

                                    case CFConstants.WIDGET_API_SUMMARY_BY:
                                    case CFConstants.WIDGET_SUMMARY_BY:
                                        widget.setSummaryBy(String.valueOf(jsonObjectWidgets.get(widgetParameter)));
                                        break;

                                    case CFConstants.WIDGET_FEED_METADATA_ID:
                                        widget.setMetadataId(String.valueOf(jsonObjectWidgets.get(widgetParameter)));
                                        break;

                                    case CFConstants.WIDGET_API_SUM:
                                        widget.setSum(String.valueOf(jsonObjectWidgets.get(widgetParameter)));
                                        break;

                                    case CFConstants.WIDGET_API_GROUPBY:
                                        widget.setGroup(String.valueOf(jsonObjectWidgets.get(widgetParameter)));
                                        break;

                                    case CFConstants.WIDGET_SIZE_X:
                                        widget.setSizeX(jsonObjectWidgets.getInt(widgetParameter));
                                        break;

                                    case CFConstants.WIDGET_SIZE_Y:
                                        widget.setSizeY(jsonObjectWidgets.getInt(widgetParameter));
                                        break;

                                    case CFConstants.WIDGET_TITLE:
                                        widget.setWidgetTitle(jsonObjectWidgets.getString(widgetParameter));
                                        break;

                                    case CFConstants.ROW:
                                        widget.setRow(jsonObjectWidgets.getInt(widgetParameter));
                                        break;

                                    case CFConstants.COL:
                                        widget.setCol(jsonObjectWidgets.getInt(widgetParameter));
                                        break;

                                    case CFConstants.IN_ROW:
                                        //widget.setInRow(jsonObjectWidgets.get(widgetParameter).toString());
                                        inRow = String.valueOf(jsonObjectWidgets.get(widgetParameter));
                                        break;

                                    case CFConstants.IN_COL:
                                        //widget.setInCol(jsonObjectWidgets.get(widgetParameter).toString());
                                        inCol = String.valueOf(jsonObjectWidgets.get(widgetParameter));
                                        break;

                                    case CFConstants.WIDGET_ISDRAWN:
                                        widget.setIsDrawn(jsonObjectWidgets.getBoolean(widgetParameter));
                                        break;

                                    case CFConstants.TOPKEYWORDS_NOOFKEYWORDS:

                                        widget.setNoOfKeywords(jsonObjectWidgets.getInt(widgetParameter));
                                        break;

                                    case CFConstants.TOPKEYWORDS_SORTBY:
                                        widget.setSortBy(jsonObjectWidgets.getString(widgetParameter));
                                        break;

                                    case CFConstants.WIDGET_PRE_FILTER_CONDITION:
                                        widget.setFilterCondition(jsonObjectWidgets.getString(widgetParameter));
                                        break;

                                    case CFConstants.WIDGET_FILTERBY_COLUMN:
                                        widget.setFilterBy(String.valueOf(jsonObjectWidgets.get(widgetParameter)).toLowerCase().replace(CFConstants.SPACE, CFConstants.EMPTY_STRING));
                                        break;

                                    case CFConstants.TOPKEYWORDS_ISCHECKED:
                                        widget.setIsChecked(jsonObjectWidgets.getBoolean(widgetParameter));
                                        break;

                                    case CFConstants.WIDGET_PRE_FILTER_BY_VALUE:
                                        widget.setFilterByValue(String.valueOf(jsonObjectWidgets.get(widgetParameter)));
                                        break;

                                    case CFConstants.WIDGET_CAMPAIGN_GROUPID:
                                        widget.setCampaignGroupId(String.valueOf(jsonObjectWidgets.get(widgetParameter)));
                                        break;

                                    case CFConstants.WIDGET_PAGESIZE:
                                        JSONObject jsonPageSize = (JSONObject) jsonObjectWidgets.get(widgetParameter);
                                        Iterator<String> iteratePageSize = jsonPageSize.keys();

                                        while (iteratePageSize.hasNext()) {
                                            String size = (String) iteratePageSize.next();
                                            if (size.equalsIgnoreCase(CFConstants.GRID_SIZE)) {
                                                widget.setWidgetGridSize((int) jsonPageSize.get(size));
                                            }
                                        }
                                        break;

                                    case CFConstants.WIDGET_CHARTOPTIONS:

                                        JSONObject jsonChartOptions = (JSONObject) jsonObjectWidgets.get(widgetParameter);
                                        Iterator<String> iterateChartOptions = jsonChartOptions.keys();

                                        while (iterateChartOptions.hasNext()) {
                                            String chart = (String) iterateChartOptions.next();

                                            if (chart.equalsIgnoreCase(CFConstants.WIDGET_CHART)) {

                                                JSONObject jsonChart = (JSONObject) jsonChartOptions.get(chart);
                                                Iterator<String> iterateChart = jsonChart.keys();

                                                while (iterateChart.hasNext()) {
                                                    String paramValue = (String) iterateChart.next();
                                                    switch (paramValue) {
                                                        case CFConstants.WIDGET_TYPE:
                                                            widget.setWidgetSubtype(String.valueOf(jsonChart.get(paramValue)));
                                                            break;

                                                        case CFConstants.WIDGET_BGCOLOR:
                                                            widget.setWidgetBGColor(String.valueOf(jsonChart.get(paramValue)));
                                                            break;
                                                        case CFConstants.WIDGET_OPTIONS3D:
                                                            widget.setOptions3d(String.valueOf(jsonChart.get(paramValue)));
                                                            break;
                                                    }
                                                }
                                            } else if (chart.equalsIgnoreCase(CFConstants.WIDGET_PLOTOPTIONS)) {
                                                JSONObject jsonPlot = (JSONObject) jsonChartOptions.get(chart);
                                                Iterator<String> iteratePlot = jsonPlot.keys();
                                                while (iteratePlot.hasNext()) {
                                                    String pie = (String) iteratePlot.next();
                                                    if (pie.equalsIgnoreCase(CFConstants.PIE)) {

                                                        JSONObject jsonPie = (JSONObject) jsonPlot.get(pie);
                                                        Iterator<String> iteratePie = jsonPie.keys();
                                                        while (iteratePie.hasNext()) {
                                                            String colors = (String) iteratePie.next();
                                                            if (colors.equalsIgnoreCase(CFConstants.COLORS)) {

                                                                widget.setColors(String.valueOf(jsonPie.get(colors)));
                                                            }
                                                        }

                                                    }

                                                }

                                            }
                                        }
                                        break;

                                    case CFConstants.WIDGET_STYLES:
                                        JSONObject jsonStyles = (JSONObject) jsonObjectWidgets.get(widgetParameter);
                                        @SuppressWarnings("unchecked") Iterator<String> iterateStyleParams = jsonStyles.keys();

                                        while (iterateStyleParams.hasNext()) {
//                                    	widget.setFilterByValue(jsonObjectWidgets.get(widgetParameter).toString());

                                            String widgetStyle = (String) iterateStyleParams.next();
                                            switch (widgetStyle) {

                                                case CFConstants.BIGNUMBER_HEADERTEXTCOLOR:

                                                    widget.setBigNumberHeaderTextColor(String.valueOf(jsonStyles.get(widgetStyle)));
                                                    break;
                                                case CFConstants.BIGNUMBER_HEADERTEXTSIZE:
                                                    widget.setBigNumberHeaderTextSize((int) jsonStyles.get(widgetStyle));
                                                    break;
                                                case CFConstants.BIGNUMBER_NUMBERTEXTCOLOR:
                                                    widget.setBigNumberNumberTextColor(String.valueOf(jsonStyles.get(widgetStyle)));
                                                    break;
                                                case CFConstants.BIGNUMBER_NUMBERTEXTSIZE:
                                                    widget.setBigNumberNumberTextSize((int) jsonStyles.get(widgetStyle));
                                                    break;
                                            }
                                        }

                                        break;

                                    case CFConstants.WIDGET_METRICS:

// ERROR USER - 9                                	
                                        if (widget.getType().equalsIgnoreCase(CFConstants.TOP_KEYWORDS)) {
                                            metrics = jsonObjectWidgets.getString(widgetParameter);

                                        } else {

                                            JSONArray metricsArray = jsonObjectWidgets.getJSONArray(widgetParameter);
                                            StringBuilder metricBuilder = new StringBuilder();

                                            for (int index = 0; index < metricsArray.length(); index++) {
                                                if (metricBuilder.length() == 0) {
                                                    metricBuilder.append(metricsArray.getString(index).replace(CFConstants.SPACE, CFConstants.EMPTY_STRING).toLowerCase());
                                                } else {
                                                    metricBuilder.append(CFConstants.COMMA).append(metricsArray.getString(index).replace(CFConstants.SPACE, CFConstants.EMPTY_STRING).toLowerCase());
                                                }
                                            }
                                            metrics = String.valueOf(metricBuilder);
                                        }
                                        widget.setMetrics(metrics);
                                        break;
                                }

                            }

                            if (widget.getRow() == -1 && widget.getCol() == -1) {
                                if (inRow != null && inCol != null && !inRow.equalsIgnoreCase(CFConstants.NULL) && !inCol.equalsIgnoreCase(CFConstants.NULL)) {
//                                    if (!inRow.equalsIgnoreCase("0") && !inCol.equalsIgnoreCase("0")) {
//
//                                    } else {
                                    widget.setRow(Integer.parseInt(inRow));
                                    widget.setCol(Integer.parseInt(inCol));
//                                    }

                                } else {
                                    widget.setRow(50);
                                    widget.setCol(0);
                                }
                            }
                            widgetList.add(widget);
                        }
                        pages.setWidget(widgetList);
                    }
                }
            }
            //}
            pagesList.add(pages);
        }
        workSpaceObj.setPage(pagesList);
        return workSpaceObj;
    }

    /**
     * Method to fetch all the widget URLs as map having widget name, type and
     * page Name as KEY for the given JSONObject
     *
     * @param jsonWSObj
     * @return
     */
    public static TreeMap<String, List<Widget>> generatedPDF(JsonWorkSpace jsonWSObj) {
        TreeMap<String, List<Widget>> pageList = new TreeMap<>();

        for (Pages pageObj : jsonWSObj.getPage()) {
            if (null != pageObj.getWidget()) {
                String pageName = pageObj.getPageName();

                List<Widget> widgetList = new ArrayList<>();
                for (Widget widgetObj : pageObj.getWidget()) {
                    if (widgetObj != null) {
                        if ((widgetObj.getMetadataId() != null && !widgetObj.getMetadataId().equalsIgnoreCase(CFConstants.NULL))
                                || (widgetObj.getCampaignGroupId() != null && !widgetObj.getCampaignGroupId().equalsIgnoreCase(CFConstants.NULL))) {
                            Widget widget = new Widget();
                            widget.setBuildUrl(URLGenerator.getWidgetUrl(widgetObj, jsonWSObj.getCompanyID(), pageName));
                            widget.setSizeX(widgetObj.getSizeX());
                            widget.setSizeY(widgetObj.getSizeY());
                            widget.setCol(widgetObj.getCol());
                            widget.setRow(widgetObj.getRow());
                            widget.setType(widgetObj.getType());
                            widget.setWidgetTitle(widgetObj.getWidgetTitle());
                            widget.setWidgetName(widgetObj.getWidgetName());
                            widget.setNoOfKeywords(widgetObj.getNoOfKeywords());
                            widget.setSortBy(widgetObj.getSortBy());
                            widget.setDefaultDate(widgetObj.getDefaultDate());
                            widget.setFilterCondition(widgetObj.getFilterCondition());
                            widget.setFilterBy(widgetObj.getFilterBy());
                            widget.setFilterByValue(widgetObj.getFilterByValue());
                            widget.setBigNumberHeaderTextColor(widgetObj.getBigNumberHeaderTextColor());
                            widget.setBigNumberHeaderTextSize(widgetObj.getBigNumberHeaderTextSize());
                            widget.setBigNumberNumberTextColor(widgetObj.getBigNumberNumberTextColor());
                            widget.setBigNumberNumberTextSize(widgetObj.getBigNumberNumberTextSize());
                            widget.setGroup(widgetObj.getGroup());
                            widget.setFields(widgetObj.getFields());
                            widget.setWidgetType(widgetObj.getWidgetType());
                            widget.setWidgetSubtype(widgetObj.getWidgetSubtype());
                            widget.setWidgetBGColor(widgetObj.getWidgetBGColor());
                            widget.setCampaignGroupId(widgetObj.getCampaignGroupId());
                            widget.setOptions3d(widgetObj.getOptions3d());
                            widget.setWidgetGridSize(widgetObj.getWidgetGridSize());
                            widget.setIsChecked(widgetObj.getIsChecked());
                            widget.setIsDrawn(widgetObj.getIsDrawn());
                            widget.setColors(widgetObj.getColors());
                            widgetList.add(widget);
                        }
                    }
                }
                pageList.put(pageName, widgetList);

            } else {
                String pageName = pageObj.getPageName();
                List<Widget> widgetList = new ArrayList<>();
                pageList.put(pageName, widgetList);
            }
        }
        return pageList;
    }

    /**
     * Method to get required Date format based on defaultValue or defaultDate
     * in json widget or return Last 7 days date by default
     *
     * @param date
     * @return
     */
    public Map<String, String> DateFormat(String date) {
        Map<String, String> dateMap = new HashMap<>();
        DateFormat dateFormat = new SimpleDateFormat(CFConstants.DATE_FORMAT);
        Date dateobj = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateobj);
        Date startFormat = null;
        Date endFormat = null;
        String startDate = null;
        String endDate = null;

        /*calendar.add(Calendar.DATE, -1);
         endFormat = calendar.getTime();
         endDate = dateFormat.format(endFormat);*/
        switch (date.toUpperCase()) {
            case CFConstants.LAST_7_DAYS:
                calendar.add(Calendar.DATE, -1);
                endFormat = calendar.getTime();
                endDate = dateFormat.format(endFormat);
                calendar.add(Calendar.DATE, -6);
                startFormat = calendar.getTime();
                startDate = dateFormat.format(startFormat);
                break;
            case CFConstants.LAST_30_DAYS:
                calendar.add(Calendar.DATE, -1);
                endFormat = calendar.getTime();
                endDate = dateFormat.format(endFormat);
                calendar.add(Calendar.DATE, -29);
                startFormat = calendar.getTime();
                startDate = dateFormat.format(startFormat);
                break;

            case CFConstants.LAST_MONTH:
                calendar.add(Calendar.MONTH, -1);
                calendar.set(Calendar.DATE, 1);
                startFormat = calendar.getTime();
                startDate = dateFormat.format(startFormat);
                calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                endFormat = calendar.getTime();
                endDate = dateFormat.format(endFormat);
                break;

            case CFConstants.THIS_MONTH:
                calendar.add(Calendar.DATE, -1);
                endFormat = calendar.getTime();
                endDate = dateFormat.format(endFormat);
                calendar.set(Calendar.DATE, 1);
                startFormat = calendar.getTime();
                startDate = dateFormat.format(startFormat);
                break;

            case CFConstants.TODAY:
                endFormat = calendar.getTime();
                endDate = dateFormat.format(endFormat);
                startDate = endDate;
                break;

            case CFConstants.YESTERDAY:
                endFormat = calendar.getTime();
                endDate = dateFormat.format(endFormat);
                calendar.add(Calendar.DATE, -1);
                startFormat = calendar.getTime();
                startDate = dateFormat.format(startFormat);
                break;

            default:
                calendar.add(Calendar.DATE, -1);
                endFormat = calendar.getTime();
                endDate = dateFormat.format(endFormat);
                calendar.add(Calendar.DATE, -29);
                startFormat = calendar.getTime();
                startDate = dateFormat.format(startFormat);
                break;
        }
        dateMap.put(CFConstants.START_DATE, startDate);
        dateMap.put(CFConstants.END_DATE, endDate);
        return dateMap;
    }
}
